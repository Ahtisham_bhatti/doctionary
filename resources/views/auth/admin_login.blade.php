<!doctype html>
<html lang="en" dir="ltr">
@include('layouts.head')
    <body class="login-img">
        <div id="global-loader" ></div>
        <div class="page">
            <div class="page-single">
                <div class="container">
                    <div class="row">
                        <div class="col col-login mx-auto">
                            <div class="text-center mb-6">
                                <img src="{{URL::to('backend/assets/images/brand/logo.png')}}" class="h-6" alt="">
                            </div>
                            <form class="card" method="post" action="{{ route('admin.login.submit') }}">
                                @csrf
                                <div class="card-body p-6">
                                    <div class="card-title text-center">Login to Admin Account</div>
                                    <div class="form-group">
                                        <label class="form-label">Email address</label>
                                        <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus>

                                @error('email')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                                    </div>
                                    <div class="form-group">
                                        <label class="form-label">Password
                                            <a href="./forgot-password.html" class="float-right small">I forgot password</a>
                                        </label>
                                         <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="current-password">

                                @error('password')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                                    </div>
                                    <div class="form-group">
                                        <label class="custom-control custom-checkbox">
                                             <input class="form-check-input" type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>
                                            <span class="custom-control-label">Remember me</span>
                                        </label>
                                    </div>
                                    <div class="form-footer">
                                        <button type="submit" class="btn btn-primary btn-block">Sign in</button>
                                    </div>
                                    <div class="text-center text-muted mt-3">
                                        Don't have account yet? <a href="./register.html">Sign up</a>
                                    </div>
                                </div>
                                
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        
        
        @include('layouts.scripts')
    </body>
</html>
 