<!doctype html>
<html lang="en" dir="ltr">
	@include('layouts.head')
	<body class="app sidebar-mini rtl">
		<div id="global-loader" ></div>
		<div class="page">
			<div class="page-main">
				<!-- Navbar-->
				@include('layouts.header')

				<!-- Sidebar menu-->
				<div class="app-sidebar__overlay" data-toggle="sidebar"></div>
				@include('layouts.sidebar')
				<div class="app-content my-3 my-md-5">
					@yield('content')
					@include('layouts.footer')
				</div>
			</div>
		</div>

		<!-- Back to top -->
		<a href="#top" id="back-to-top" style="display: inline;"><i class="fa fa-angle-up"></i></a>

		<!-- Dashboard Core -->
	@include('layouts.scripts')

	</body>
</html>