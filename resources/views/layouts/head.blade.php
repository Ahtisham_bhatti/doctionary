<head>
		<meta charset="UTF-8">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">

		<meta name="msapplication-TileColor" content="#0061da">
		<meta name="theme-color" content="#1643a3">
		<meta name="apple-mobile-web-app-status-bar-style" content="black-translucent"/>
		<meta name="apple-mobile-web-app-capable" content="yes">
		<meta name="mobile-web-app-capable" content="yes">
		<meta name="HandheldFriendly" content="True">
		<meta name="MobileOptimized" content="320">
		<link rel="icon" href="{{URL::to('backend/assets/images/brand/logo.png')}}" type="image/png"/>
		<link rel="shortcut icon" type="image/png" href="{{URL::to('backend/assets/images/brand/logo.png')}}" />

		<!-- Title -->
		<title>MyDoctionary</title>
		<link rel="stylesheet" href="{{URL::to('backend/assets/fonts/fonts/font-awesome.min.css')}}">

		<!-- Font Family-->
		<link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800" rel="stylesheet">

		<!-- Dashboard Css -->

		<link href="{{URL::to('backend/assets/css/dashboard.css')}}" rel="stylesheet" />

		<!-- Custom scroll bar css-->
		<link href="{{URL::to('backend/assets/plugins/scroll-bar/jquery.mCustomScrollbar.css')}}" rel="stylesheet" />

		<!-- Sidemenu Css -->
		<link href="{{URL::to('backend/assets/plugins/toggle-sidebar/css/sidemenu.css')}}" rel="stylesheet">

		<!-- c3.js Charts Plugin -->
		<link href="{{URL::to('backend/assets/plugins/charts-c3/c3-chart.css')}}" rel="stylesheet" />
		<link href="{{URL::to('backend/assets/plugins/morris/morris.css')}}" rel="stylesheet" />

		<!-- Data table css -->
		<link href="{{URL::to('backend/assets/plugins/datatable/dataTables.bootstrap4.min.css')}}" rel="stylesheet" />

		<!-- Slect2 css -->
		<link href="{{URL::to('backend/assets/plugins/select2/select2.min.css')}}" rel="stylesheet" />

		<!---Font icons-->
		<link href="{{URL::to('backend/assets/plugins/iconfonts/plugin.css')}}" rel="stylesheet" />
	</head>