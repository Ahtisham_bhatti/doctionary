<a href="#top" id="back-to-top" style="display: inline;"><i class="fa fa-angle-up"></i></a>
	<script src="{{URL::to('backend/assets/js/vendors/jquery-3.2.1.min.js')}}"></script>
		<script src="{{URL::to('backend/assets/js/vendors/bootstrap.bundle.min.js')}}"></script>

		<script src="{{URL::to('backend/assets/js/vendors/jquery.sparkline.min.js')}}"></script>
		<script src="{{URL::to('backend/assets/js/vendors/selectize.min.js')}}"></script>
		<script src="{{URL::to('backend/assets/js/vendors/jquery.tablesorter.min.js')}}"></script>
		<script src="{{URL::to('backend/assets/js/vendors/circle-progress.min.js')}}"></script>
		<script src="{{URL::to('backend/assets/plugins/rating/jquery.rating-stars.js')}}"></script>
		<!-- Side menu js -->
		<script src="{{URL::to('backend/assets/plugins/toggle-sidebar/js/sidemenu.js')}}"></script>

		<!-- Custom scroll bar Js-->
		<script src="{{URL::to('backend/assets/plugins/scroll-bar/jquery.mCustomScrollbar.concat.min.js')}}"></script>

		<!-- Timeline js -->
		<script src="{{URL::to('backend/assets/plugins/timeline/timeline.min.js')}}"></script>
		<script>
			timeline(document.querySelectorAll('.timeline'), {
				forceVerticalMode: 700,
				mode: 'horizontal',
				verticalStartPosition: 'left',
				visibleItems: 4
			});
		</script>

		
		<!-- Chat js -->
		<script src="{{URL::to('backend/assets/plugins/chat/jquery.convform.js')}}"></script>
		<script src="{{URL::to('backend/assets/plugins/chat/autosize.min.js')}}"></script>
		<script src="{{URL::to('backend/assets/js/chat.js')}}"></script>

			<!--Rang slider -->
		<script src="{{URL::to('backend/assets/plugins/range/addSlider.min.js')}}"></script>
		<script src="{{URL::to('backend/assets/plugins/range/Obj.min.js')}}"></script>

		<script >
			function betterParseFloat(x){
			  if(isNaN(parseFloat(x)) && x.length > 0)
				return betterParseFloat(x.substr(1));
			  return parseFloat(x);
			}
			function usd(x){
			  x = betterParseFloat(x);
			  if(isNaN(x))
				return "$0.00";
			  var dollars = Math.floor(x);
			  var cents = Math.round((x - dollars) * 100) + "";
			  if(cents.length==1)cents = "0"+cents;
			  return "$"+dollars+"."+cents;
			}
		</script>

			<!-- Data tables -->
		<script src="{{URL::to('backend/assets/plugins/datatable/jquery.dataTables.min.js')}}"></script>
		<script src="{{URL::to('backend/assets/plugins/datatable/dataTables.bootstrap4.min.js')}}"></script>


		<!--Select2 js -->
		<!-- <script src="{{URL::to('backend/assets/plugins/select2/select2.full.min.js')}}"></script>
 -->			<!-- Inline js -->
		<script src="{{URL::to('backend/assets/js/select2.js')}}"></script>



		<!-- c3.js Charts Plugin -->
		<script src="{{URL::to('backend/assets/plugins/charts-c3/d3.v5.min.js')}}"></script>
		<script src="{{URL::to('backend/assets/plugins/charts-c3/c3-chart.js')}}"></script>

		  <!-- Index Scripts -->
		<script src="{{URL::to('backend/assets/js/index.js')}}"></script>
		<script src="{{URL::to('backend/assets/js/charts.js')}}"></script>





	


<!--Morris.js Charts Plugin -->
<script src="{{URL::to('backend/assets/plugins/morris/raphael-min.js')}}"></script>
<script src="{{URL::to('backend/assets/plugins/morris/morris.js')}}"></script>

<!--Morris.js Charts Plugin -->
<script src="{{URL::to('backend/assets/plugins/chart/Chart.bundle.js')}}"></script>
<script src="{{URL::to('backend/assets/plugins/chart/utils.js')}}"></script>

<!-- Input Mask Plugin -->
		<script src="{{URL::to('backend/assets/plugins/input-mask/jquery.mask.min.js')}}"></script>


	

<script src="{{URL::to('backend/assets/js/morris.js')}}"></script>

<!-- Accordion Scripts -->
<script src="{{URL::to('backend/assets/plugins/accordion/accordion.min.js')}}"></script>

<script>
	$(function(e) {
		$(".demo-accordion").accordionjs();
		// Demo text. Let's save some space to make the code readable. ;)
		$('.acc_content').not('.nodemohtml').html('<p>Quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Fusce aliquet neque et accumsan fermentum. Aliquam lobortis neque in nulla  tempus, molestie fermentum purus euismod.</p>');
	});
</script>

	<!---Tabs JS-->
		<script src="{{URL::to('backend/assets/plugins/tabs/jquery.multipurpose_tabcontent.js')}}"></script>





			<!-- Timepicker js -->
		<script src="{{URL::to('backend/assets/plugins/time-picker/jquery.timepicker.js')}}"></script>
		<script src="{{URL::to('backend/assets/plugins/time-picker/toggles.min.js')}}"></script>

		<!-- Datepicker js -->
		<script src="{{URL::to('backend/assets/plugins/date-picker/spectrum.js')}}"></script>
		<script src="{{URL::to('backend/assets/plugins/date-picker/jquery-ui.js')}}"></script>
		<script src="{{URL::to('backend/assets/plugins/input-mask/jquery.maskedinput.js')}}"></script>
	

		

		<!-- forn-wizard js-->
		<script src="{{URL::to('backend/assets/plugins/forn-wizard/js/material-bootstrap-wizard.js')}}"></script>
		<script src="{{URL::to('backend/assets/plugins/forn-wizard/js/jquery.validate.min.js')}}"></script>
		<script src="{{URL::to('backend/assets/plugins/forn-wizard/js/jquery.bootstrap.js')}}"></script>
	



		

      
	


		<!-- custom js -->
		<script src="{{URL::to('backend/assets/js/custom.js')}}"></script>
			<script>
			$(function(e) {
				$('#example').DataTable();
			} );
		</script>

			<!---Tabs scripts-->
		<script>
			$(function(e) {
				$(".first_tab").champ();
				$(".accordion_example").champ({
					plugin_type: "accordion",
					side: "left",
					active_tab: "3",
					controllers: "true"
				});

				$(".second_tab").champ({
					plugin_type: "tab",
					side: "right",
					active_tab: "1",
					controllers: "false"
				});

			});
		</script>
