<aside class="app-sidebar">
					<div class="app-sidebar__user">
						<div class="dropdown">
							<a class="nav-link p-0 leading-none d-flex" data-toggle="dropdown" href="#">
								<span class="avatar avatar-md brround" style="background-image: url(backend/assets/images/faces/female/25.jpg)"></span>
								<span class="ml-2 "><span class="text-dark app-sidebar__user-name font-weight-semibold">Doctionry</span><br>
									<span class="text-muted app-sidebar__user-name text-sm"> Ui Designer</span>
								</span>
							</a>
							<div class="dropdown-menu dropdown-menu-right dropdown-menu-arrow">
								<a class="dropdown-item" href="#"><i class="dropdown-icon mdi mdi-account-outline"></i> Profile</a>
								<a class="dropdown-item" href="#"><i class="dropdown-icon mdi mdi-settings"></i> Settings</a>
								<a class="dropdown-item" href="#"><span class="float-right"><span class="badge badge-primary">6</span></span> <i class="dropdown-icon mdi mdi-message-outline"></i> Inbox</a>
								<a class="dropdown-item" href="#"><i class="dropdown-icon mdi mdi-comment-check-outline"></i> Message</a>
								<div class="dropdown-divider"></div>
								<a class="dropdown-item" href="#"><i class="dropdown-icon mdi mdi-compass-outline"></i> Need help?</a>
								<a class="dropdown-item" href="login.html"><i class="dropdown-icon mdi mdi-logout-variant"></i> Sign out</a>
							</div>
						</div>
					</div>
					<ul class="side-menu">
						<li>
							<a class="side-menu__item @if(request()->is('dashboard')) active @endif" href="{{ route('admin.dashboard') }}"><i class="side-menu__icon fa fa-home"></i><span class="side-menu__label">Dashboard</span></a>
						</li>
						<li>
							<a class="side-menu__item @if(request()->is('lab/appointment')) active @endif" href="{{ route('lab.appointment') }}"><i class="side-menu__icon fa fa-flask"></i><span class="side-menu__label">Manage Labs</span></a>
						</li>
						<li>
							<a class="side-menu__item @if(request()->is('pharmacy/order')) active @endif" href="{{ route('pharmacy.order') }}"><i class="side-menu__icon fa fa-medkit"></i><span class="side-menu__label">Manage Pharmacy</span></a>
						</li>
						<li>
							<a class="side-menu__item @if(request()->is('hospital/appointment')) active @endif" href="{{ route('hospital.appointment') }}"><i class="side-menu__icon fa fa-hospital-o"></i><span class="side-menu__label">Manage Hospital</span></a>
						</li>
						<li>
							<a class="side-menu__item @if(request()->is('doctor/appointment')) active @endif" href="{{ route('doctor.appointment') }}"><i class="side-menu__icon fa fa fa-user-md"></i><span class="side-menu__label">Manage Doctor</span></a>
						</li>
						
						<li>
							<a class="side-menu__item" href="https://themeforest.net/user/sprukotechnologies/portfolio"><i class="side-menu__icon fa fa-question-circle"></i><span class="side-menu__label">Help & Support</span></a>
						</li>
					</ul>
				</aside>