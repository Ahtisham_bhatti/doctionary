<!DOCTYPE html>
<html>
  
<!-- Mirrored from wahabali.com/work/medical-guide/index.html by HTTrack Website Copier/3.x [XR&CO'2014], Mon, 11 Nov 2019 14:15:16 GMT -->
@include('frontend.layouts.head')
  <body>
    
  
  
  <div id="wrap">
   
  <!--Start PreLoader-->
   <div id="preloader">
    <div id="status">&nbsp;</div>
    <div class="loader">
      <h1>Loading...</h1>
      <span></span>
      <span></span>
      <span></span>
    </div>
  </div>
  <!--End PreLoader-->

  <div id="header-2" style="display:none">
   <header class="header header2">
   
     @include('frontend.layouts.header')   
   </header>
  </div>
  
  
   <!--Start Header-->
   <div id="header-1">
   <!--Start Top Bar-->
   @include('frontend.layouts.top_bar')
   <!--Top Bar End-->
   
   
   <!--Start Header-->
   
   <header class="header">
    @include('frontend.layouts.header')  
    </header>
  </div>
    
   <!--End Header-->
  
  
  <!-- Mobile Menu Start -->
 @include('frontend.layouts.mobile_menu')
    <!-- Mobile Menu End -->
  
  

   
   <!--End Banner-->
   
   
   
   
   <!--Start Make Appointment-->
   
   <!--End Make Appointment-->  
    
    
   
   <!--Start Content-->
  @yield('content')
   <!--End Content-->
   
   
   
   
   <!--Start Footer-->
  @include('frontend.layouts.footer')
   <!--End Footer-->



<a href="#0" class="cd-top"></a>
  </div>




@include('frontend.layouts.scripts')

</body>

<!-- Mirrored from wahabali.com/work/medical-guide/index.html by HTTrack Website Copier/3.x [XR&CO'2014], Mon, 11 Nov 2019 14:19:51 GMT -->
</html>