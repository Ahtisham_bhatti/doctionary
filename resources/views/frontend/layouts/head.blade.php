<head>
  <title>MyDoctionary</title>
  <meta name="keywords" content="">
	<meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no">
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">


	<link rel="icon" type="image/png" href="{{URL::to('frontend/images/logo.png')}}">
	
    <!--main file-->
	<link href="{{URL::to('frontend/css/medical-guide.css')}}" rel="stylesheet" type="text/css">
    
    <!--Medical Guide Icons-->
	<link href="{{URL::to('frontend/fonts/medical-guide-icons.css')}}" rel="stylesheet" type="text/css">
	
	
	<!-- Default Color-->
	<link href="{{URL::to('frontend/css/default-color.css')}}" rel="stylesheet" id="color"  type="text/css">
    
    <!--bootstrap-->

	<link href="{{URL::to('frontend/css/bootstrap.css')}}" rel="stylesheet" type="text/css">


    
    <!--Dropmenu-->
	<link href="{{URL::to('frontend/css/dropmenu.css')}}" rel="stylesheet" type="text/css">
    
	<!--Sticky Header-->
	<link href="{{URL::to('frontend/css/sticky-header.css')}}" rel="stylesheet" type="text/css">
	
	<!--revolution-->
	<link href="{{URL::to('frontend/css/style.css')}}" rel="stylesheet" type="text/css">    
    <link href="{{URL::to('frontend/css/settings.css')}}" rel="stylesheet" type="text/css">    
    <link href="{{URL::to('frontend/css/extralayers.css')}}" rel="stylesheet" type="text/css">    
   
    <!--Accordion-->
    <link href="{{URL::to('frontend/css/accordion.css')}}" rel="stylesheet" type="text/css">
     
    <!--tabs-->
	<link href="{{URL::to('frontend/css/tabs.css')}}" rel="stylesheet" type="text/css">    
   
    <!--Owl Carousel-->
	<link href="{{URL::to('frontend/css/owl.carousel.css')}}" rel="stylesheet" type="text/css">    
	
    <!-- Mobile Menu -->
	<link rel="stylesheet" type="text/css" href="{{URL::to('frontend/css/jquery.mmenu.all.css')}}" />
	<link rel="stylesheet" type="text/css" href="{{URL::to('frontend/css/demo.css')}}" />
	
	<!--PreLoader-->
	<link href="{{URL::to('frontend/css/loader.css')}}" rel="stylesheet" type="text/css">    
   
    <!--switcher-->
	<link href="{{URL::to('frontend/css/switcher.css')}}" rel="stylesheet" type="text/css">

	

	
	
</head>