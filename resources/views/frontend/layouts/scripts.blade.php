<script type="text/javascript" src="{{URL::to('frontend/js/jquery.js')}}"></script>

<!-- SMOOTH SCROLL -->  
<script type="text/javascript" src="{{URL::to('frontend/js/scroll-desktop.js')}}"></script>
<script type="text/javascript" src="{{URL::to('frontend/js/scroll-desktop-smooth.js')}}"></script>

<!-- START REVOLUTION SLIDER -->  
<script type="text/javascript" src="{{URL::to('frontend/js/jquery.themepunch.revolution.min.js')}}"></script>
<script type="text/javascript" src="{{URL::to('frontend/js/jquery.themepunch.tools.min.js')}}"></script>


<!-- Date Picker and input hover -->
<script type="text/javascript" src="{{URL::to('frontend/js/classie.js')}}"></script> 
<script type="text/javascript" src="{{URL::to('frontend/js/jquery-ui-1.10.3.custom.js')}}"></script>

<!-- Fun Facts Counter -->
<script type="text/javascript" src="{{URL::to('frontend/js/counter.js')}}"></script>


<!-- Welcome Tabs --> 
<script type="text/javascript" src="{{URL::to('frontend/js/tabs.js')}}"></script>


<!-- All Carousel -->
<script type="text/javascript" src="{{URL::to('frontend/js/owl.carousel.js')}}"></script>
<!-- Easy responsive tabs -->
<script type="text/javascript" src="{{URL::to('frontend/js/easy-responsive-tabs.js')}}"></script> 


<!-- RS slides -->
<script type="text/javascript" src="{{URL::to('frontend/js/rs-slides.js')}}"></script> 

<script type="text/javascript" src="{{URL::to('frontend/js/price-slider.js')}}"></script> 

<script>
(function () {
			"use strict";
/* ------------------------------------------------------------------------ 
			   ADD REVIEW CUSTOM SCRIPT [open/close]
			------------------------------------------------------------------------ */
			jQuery("#add-review-btn").click(function(){
				jQuery("#add-review-form").slideDown();
			});
			jQuery("#review-form-close").click(function(){
				jQuery("#add-review-form").slideUp();
			});
			
			
			
			/* ------------------------------------------------------------------------ 
			   PRODUCT DETAIL
			------------------------------------------------------------------------ */
			jQuery("#product-detail-slider").responsiveSlides({
				maxwidth: 1000,
				timeout: 3E3,
				pager: true,
				speed: 700
			});
			/* ------------------------------------------------------------------------ 
			   VERTICAL TABS [with bordered nav]
			------------------------------------------------------------------------ */
			jQuery('.verticalTab').easyResponsiveTabs({
				type: 'vertical',
				width: 'auto',
				fit: true
			});
			
			
			/* ------------------------------------------------------------------------ 
			   ITEM COUNTER CUSTOM SCRIPT
			------------------------------------------------------------------------ */
			var itemcount= 0;

			$('#pluss-item').on("click", function() { 
				itemcount++;
				$('#total-items').val(itemcount);
				return false;
			});

			$('#less-item').on("click", function() { 
				itemcount--;
				$('#total-items').val(itemcount);
				return false;
			});
			
			$('.verticalTab').easyResponsiveTabs({
				type: 'vertical',
				width: 'auto',
				fit: true
			});
			


/* ------------------------------------------------------------------------ 
					   RANGE SLIDER [price filter]
------------------------------------------------------------------------ */
					jQuery( "#slider-range" ).slider({
					range: true,
					min: 24781,
					max: 50000,
					values: [ 28781,45000],
					
					slide: function( event, ui ) {
						
						jQuery( "#amount" ).val( "$" + ui.values[ 0 ] + " - $" + ui.values[ 1 ] );
					},
					
					stop: function(event, ui) {
					   jQuery( "#sort_price_max" ).val(ui.values[ 1 ] );
					   jQuery( "#sort_price_min" ).val(ui.values[ 0 ] );
					}
					});
					jQuery( "#amount" ).val( "$" + jQuery( "#slider-range" ).slider( "values", 0 ) +
					" - $" + jQuery( "#slider-range" ).slider( "values", 1 ) );
					})(jQuery);
</script>

<!-- Mobile Menu -->
<script type="text/javascript" src="{{URL::to('frontend/js/jquery.mmenu.min.all.js')}}"></script>

<!-- All Scripts -->
<script type="text/javascript" src="{{URL::to('frontend/js/custom.js')}}"></script> 

<script>
	<!-- Fancybox -->	
/*
             *  Simple image gallery. Uses default settings
             */
    
            $('.fancybox').fancybox();
    
            /*
             *  Different effects
             */
    
            $(document).ready(function() {
			$('.fancybox-media').fancybox({
				openEffect  : 'none',
				closeEffect : 'none',
				helpers : {
					media : {}
				}
			});
			});
</script>


<!-- Revolution Slider -->  
<script type="text/javascript">
jQuery('.tp-banner').show().revolution(
{
dottedOverlay:"none",
delay:16000,
startwidth:1170,
startheight:720,
hideThumbs:200,

thumbWidth:100,
thumbHeight:50,
thumbAmount:5,

navigationType:"nexttobullets",
navigationArrows:"solo",
navigationStyle:"preview",

touchenabled:"on",
onHoverStop:"on",

swipe_velocity: 0.7,
swipe_min_touches: 1,
swipe_max_touches: 1,
drag_block_vertical: false,

parallax:"mouse",
parallaxBgFreeze:"on",
parallaxLevels:[7,4,3,2,5,4,3,2,1,0],

keyboardNavigation:"off",

navigationHAlign:"center",
navigationVAlign:"bottom",
navigationHOffset:0,
navigationVOffset:20,

soloArrowLeftHalign:"left",
soloArrowLeftValign:"center",
soloArrowLeftHOffset:20,
soloArrowLeftVOffset:0,

soloArrowRightHalign:"right",
soloArrowRightValign:"center",
soloArrowRightHOffset:20,
soloArrowRightVOffset:0,

shadow:0,
fullWidth:"on",
fullScreen:"off",

spinner:"spinner4",

stopLoop:"off",
stopAfterLoops:-1,
stopAtSlide:-1,

shuffle:"off",

autoHeight:"off",           
forceFullWidth:"off",           



hideThumbsOnMobile:"off",
hideNavDelayOnMobile:1500,            
hideBulletsOnMobile:"off",
hideArrowsOnMobile:"off",
hideThumbsUnderResolution:0,

hideSliderAtLimit:0,
hideCaptionAtLimit:0,
hideAllCaptionAtLilmit:0,
startWithSlide:0,
videoJsPath:"rs-plugin/videojs/",
fullScreenOffsetContainer: "" 
});
</script><!-- Revolution Slider --> 
<script type="text/javascript">
jQuery('.tp-banner').show().revolution(
{
dottedOverlay:"none",
delay:16000,
startwidth:1170,
startheight:720,
hideThumbs:200,

thumbWidth:100,
thumbHeight:50,
thumbAmount:5,

navigationType:"nexttobullets",
navigationArrows:"solo",
navigationStyle:"preview",

touchenabled:"on",
onHoverStop:"on",

swipe_velocity: 0.7,
swipe_min_touches: 1,
swipe_max_touches: 1,
drag_block_vertical: false,

parallax:"mouse",
parallaxBgFreeze:"on",
parallaxLevels:[7,4,3,2,5,4,3,2,1,0],

keyboardNavigation:"off",

navigationHAlign:"center",
navigationVAlign:"bottom",
navigationHOffset:0,
navigationVOffset:20,

soloArrowLeftHalign:"left",
soloArrowLeftValign:"center",
soloArrowLeftHOffset:20,
soloArrowLeftVOffset:0,

soloArrowRightHalign:"right",
soloArrowRightValign:"center",
soloArrowRightHOffset:20,
soloArrowRightVOffset:0,

shadow:0,
fullWidth:"on",
fullScreen:"off",

spinner:"spinner4",

stopLoop:"off",
stopAfterLoops:-1,
stopAtSlide:-1,

shuffle:"off",

autoHeight:"off",           
forceFullWidth:"off",           



hideThumbsOnMobile:"off",
hideNavDelayOnMobile:1500,            
hideBulletsOnMobile:"off",
hideArrowsOnMobile:"off",
hideThumbsUnderResolution:0,

hideSliderAtLimit:0,
hideCaptionAtLimit:0,
hideAllCaptionAtLilmit:0,
startWithSlide:0,
videoJsPath:"rs-plugin/videojs/",
fullScreenOffsetContainer: "" 
});
</script>
