 <footer class="footer" id="footer">
      <div class="container">
          <div class="row">
              <div class="col-md-12">
                  <div class="emergency">
                      <i class="icon-phone5"></i>
                        <span class="text">For emergency cases</span>
                        <span class="number">1-300-400-8211</span>
                        <img src="images/emergency-divider.png" alt="">
                    </div>
                </div>
            </div>
            
            
            <div class="main-footer">
              <div class="row">
                  
                    <div class="col-md-3">
                      
                        <div class="useful-links">
                          <div class="title">
                              <h5>DOCTIONARY</h5>
                            </div>
                            
                            <div class="detail">
                              <ul>
                                  
                                  <li><a href="{{route('home')}}">Home</a></li>
                                    <li><a href="{{route('web.pharmacies')}}">Pharmacies</a></li>
                                    <li><a href="{{route('web.labs')}}">LabTest</a></li>
                                    <li><a href="{{route('web.doctors')}}">FindDoctors</a></li>
                                    <li><a href="{{route('web.hospitals')}}">Hospitals</a></li>
                                    <li><a href="{{route('web.shop')}}">Shop</a></li>
                                    <li><a href="{{route('web.contact')}}">Contact Us</a></li>
                                    
                                </ul>
                            </div>
                            
                        </div>
                        
                    </div>
                    <div class="col-md-3">
                      
                        
                          <div class="title">
                              <h5>LATEST TWEETS</h5>
                            </div>
                            
                            <div class="detail">
                              
                                <div class="tweets">
                                  
                                    <div class="icon">
                                      <i class="icon-yen"></i>
                                    </div>
                                    
                                    <div class="text">
                                      <p><a href="#.">@Rotography</a> Please kindly use Support Forum: <a href="#.">Doctionary.co.uk</a></p>
                                        <span>3 days ago</span>
                                    </div>
                                    
                                </div>
                                
                                <div class="tweets">
                                  
                                    <div class="icon">
                                      <i class="icon-yen"></i>
                                    </div>
                                    
                                    <div class="text">
                                      <p>A quick launcher for WordPress dashboard <a href="#.">@DOCTIONARY</a></p>
                                        <span>about a week ago</span>
                                    </div>
                                    
                                </div>
                                
                            </div>
                            
                        
                        
                    </div>
                    <div class="col-md-3">
                      
                        <div class="newsletter">
                          <div class="title">
                              <h5>NEWSLETTER</h5>
                            </div>
                            
                            <div class="detail">
                              
                                    <div class="signup-text">
                                      <i class="icon-dollar"></i>
                                    <span>Sign up with your name and email to get updates fresh updates.</span>
                                    </div>
                                    
                                    <div class="form">
                                    <p class="subscribe_success" id="subscribe_success" style="display:none;"></p>
                                    <p class="subscribe_error" id="subscribe_error" style="display:none;"></p>
                                    
                                    <form name="subscribe_form" id="subscribe_form" method="post" action="#" onSubmit="return false">
                                      <input type="text" data-delay="300" placeholder="Your Name" name="subscribe_name" id="subscribe_name" onKeyPress="removeChecks();" class="input" >
                                      <input type="text" data-delay="300" placeholder="Email Address" name="subscribe_email" id="subscribe_email" onKeyPress="removeChecks();" class="input" >
                                        <input name="Subscribe" type="submit" value="Subscribe" onClick="validateSubscription();">
                                    </form>
                                    </div>
                                    
                                </div>
                            
                            
                        </div>
                        
                    </div>
                    <div class="col-md-3">
                      
                        <div class="get-touch">
                          <div class="title">
                              <h5>GET IN TOUCH</h5>
                            </div>
                            
                            <div class="detail">
                              <div class="get-touch">
                                  
                                    
                                    <span class="text">Medical Bibendum auctor, to consequat ipsum, nec sagittis sem</span>
                                    
                                    
                                    <ul>
                                      <li><i class="icon-location"></i> <span>Doctionary Ltd, Manhattan 1258, New York, USA Lahore</span></li>
                                        <li><i class="icon-phone4"></i> <span>(+1) 234 567 8901</span></li>
                                        <li><a href="#."><i class="icon-dollar"></i> <span>hello@doctionary.com</span></a></li>
                                    </ul> 
                                    
                                </div>
                            </div>
                            
                        </div>
                        
                    </div>
                    
                </div>
                
            </div>
            
        </div>
        
        <div class="footer-bottom">
          <div class="container">
              <div class="row">
                  
                    <div class="col-md-6">
                      <span class="copyrights">Copyright &copy; 2019 Doctionary. All right reserved.</span>
                    </div>
                    
                    <div class="col-md-6">
                      <div class="social-icons">
                          <a href="#." class="fb"><i class="icon-euro"></i></a>
                            <a href="#." class="tw"><i class="icon-yen"></i></a>
                            <a href="#." class="gp"><i class="icon-google-plus"></i></a>
                            <a href="#." class="vimeo"><i class="icon-vimeo4"></i></a>
                        </div>
                    </div>
                    
                </div>
            </div>
            
        </div>
        
   </footer>