<div class="container">
      
      
        <div class="row">
          
            <div class="col-md-3">
              <a href="{{route('home')}}" class="logo"><img src="{{URL::to('frontend/images/logo.png')}}" alt=""n style="width: 130px;"></a>
            </div>
            
            <div class="col-md-9">
              
                
                <nav class="menu-2">
              <ul class="nav wtf-menu">

                  <li class="@if(request()->is('/')) item-select @endif parent "><a href="{{route('home')}}">Home</a>
         
          </li>
          
                    <li class=" @if(request()->is('pharmacies')) item-select @endif parent "><a href="{{route('web.pharmacies')}}">Pharmacies</a>
          
          </li>
          
                    <li class="@if(request()->is('labs')) item-select @endif parent"><a href="{{route('web.labs')}}">Lab Test</a>
                      
                    </li>
          
                    <li class="@if(request()->is('doctors')) item-select @endif parent"><a href="{{route('web.doctors')}}">Find Doctors</a></li>
          
                    <li class="@if(request()->is('hospitals')) item-select @endif parent"><a href="{{route('web.hospitals')}}">Hospitals</a>
            
          </li>
                    
         
          
                    <li class="@if(request()->is('shop')) item-select @endif parent"><a href="{{route('web.shop')}}">Shop</a></li>
                    
          <li class="@if(request()->is('contact-us')) item-select @endif parent"><a href="{{route('web.contact')}}">Contact Us</a>
          
          </li>
          
                </ul>
                </nav>
                  
            </div>
            
        </div>
        </div>