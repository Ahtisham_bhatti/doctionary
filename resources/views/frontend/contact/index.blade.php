@extends('frontend.master')

@section('content')

<!-- Mobile Menu Start -->
	<div class="container">
    <div id="page">
			<header class="header">
				<a href="#menu"></a>
				
			</header>
			
			<nav id="menu">
				<ul>
					<li><a href="#.">Home</a>
                    	<ul>
							<li> <a href="index.html">Home Page 1</a> </li>
							<li> <a href="index2.html">Home Page 2</a> </li>
							<li> <a href="index3.html">Home Page 3</a> </li>
							<li> <a href="index4.html">Home Page 4</a> </li>
                        </ul>
                    </li>
					<li><a href="#.">About us</a>
                    	<ul>
                        	<li> <a href="about-us.html">About Us</a> </li>
							<li> <a href="about-us2.html">About Us 2</a> </li>
                        </ul>
                    </li>
                    <li><a href="#.">Pages</a>
                    	<ul>
                        	<li> <a href="services.html">Services</a> </li>
							<li> <a href="services2.html">Services Two</a> </li>
							<li> <a href="appointment.html">Appointment</a> </li>
							<li> <a href="departments.html">Departments</a> </li>
							<li> <a href="patient-and-family.html">Patient and Family</a> </li>
							<li> <a href="team-members.html">Team Members One</a> </li>
							<li> <a href="team-members2.html">Team Members Two</a> </li>
							<li> <a href="research.html">Research</a> </li>
							<li> <a href="tables.html">Pricing tabels</a> </li>
                        </ul>
                    </li>
                    
                    <li><a href="procedures.html">Procedures</a></li>
					
					<li><a href="#.">Gallery</a>
                    	
						<ul>
                        	<li><a href="#.">Simple Gallery</a>
                            	<ul>
                                	<li> <a href="gallery-simple-two.html">Columns Two</a> </li>
                                    <li> <a href="gallery-simple-three.html">Columns Three</a> </li>
                                    <li> <a href="gallery-simple-four.html">Columns Four</a> </li>
                                </ul>
                            </li>
							
							<li><a href="#.">Nimble Gallery</a>
                            	<ul>
                                	<li> <a href="gallery-nimble-two.html">Columns Two</a> </li>
                                    <li> <a href="gallery-nimble-three.html">Columns Three</a> </li>
                                    <li> <a href="gallery-nimble-four.html">Columns Four</a> </li>
                                </ul>
                            </li>
                        </ul>
						
                    </li>
                    
                    
                    <li><a href="#.">News Posts</a>
                    	<ul>
                        	<li> <a href="news-sidebar.html">Sidebar</a> </li>
							<li> <a href="news-text.html">Text-Based</a> </li>
							<li> <a href="news-single.html">Single Post</a> </li>
							<li> <a href="news-double.html">Double Post</a> </li>
							<li> <a href="news-masonary.html">Masonary</a> </li>
                        </ul>
                    </li>
                    
					<li><a href="shop.html">Shop</a></li>
					
					<li class="select"><a href="#.">Contact Us</a>
                    	<ul>
                        	<li> <a href="contact-us.html">Contact-Us One</a> </li>
							<li class="select"> <a href="contact-us2.html">Contact-Us Two</a> </li>
                        </ul>
                    </li>
					
				</ul>
                
                
			</nav>
		</div>
		</div>
    <!-- Mobile Menu End -->

	
   <!--Start Banner-->
   
   <div class="sub-banner">
   
   	<img class="banner-img" src="images/sub-banner.jpg" alt="">
    <div class="detail">
    	<div class="container">
        	<div class="row">
            	<div class="col-md-12">
                	
                    <div class="paging">
                		<h2>Contact Us</h2>
						<ul>
						<li><a href="index.html">Home</a></li>
						<li><a>Contact Us</a></li>
						</ul>
                    </div>
                    
                </div>
            </div>
        </div>
    </div>
   
   </div>	
   
   <!--End Banner-->
   
    
   
   <!--Start Content-->
   <div class="content">
   
   
  
  <div class="contact-us">
   		<div class="container">
        	
            <div class="row">
            	<div class="col-md-12">
				
				<div class="our-location">
				<div class="map"><iframe src="https://maps.google.com/maps?f=q&amp;source=s_q&amp;hl=en&amp;geocode=&amp;q=Envato+Pty+Ltd,+Elizabeth+Street,+Melbourne,+Victoria,+Australia&amp;aq=0&amp;oq=envato+&amp;sll=37.0625,-95.677068&amp;sspn=39.235538,86.572266&amp;ie=UTF8&amp;hq=Envato+Pty+Ltd,&amp;hnear=Elizabeth+St,+Melbourne+Victoria+3000,+Australia&amp;ll=-37.817942,144.964977&amp;spn=0.01918,0.008866&amp;t=m&amp;output=embed"></iframe></div>
                    	<div class="get-directions">
                        	<form action="http://maps.google.com/maps" method="get" target="_blank">
                               <input type="text" name="saddr" placeholder="Enter Your Address" />
                               <input type="hidden" name="daddr" value="Envato Pty Ltd, Elizabeth Street, Melbourne, Victoria, Australia" />
                               <input type="submit" value="Get directions" class="direction-btn" />
                            </form>
                        </div>
                </div>
				
				</div>
            </div>
            
        </div>
		
		
		<div class="leave-msg dark-back">
			<div class="container">
			
			<div class="rox">
				<div class="col-md-7">
					
					<div class="main-title">
						<h2><span>We'd</span> Love to <span>Hear From You</span></h2>
						<p>cursus lorem molestie vitae. Nulla vehicula, lacus ut suscipit fermentum, turpis felis ultricies dui, ut rhoncus libero augue. </p>
					 </div>
					
					<div class="form">
				<div class="row">
                                    <p class="success" id="success" style="display:none;"></p>
                                    <p class="error" id="error" style="display:none;"></p>
                <form name="contact_form" id="contact_form" method="post" action="#" onSubmit="return false">
					<div class="col-md-4"><input type="text" data-delay="300" placeholder="Your full name" name="contact_name" id="contact_name" class="input"></div>
					<div class="col-md-4"><input type="text" data-delay="300" placeholder="E-mail Address" name="contact_email" id="contact_email" class="input"></div>
					<div class="col-md-4"><input type="text" data-delay="300" placeholder="Subject" name="contact_subject" id="contact_subject" class="input"></div>
					<div class="col-md-12"><textarea data-delay="500" class="required valid" placeholder="Message" name="message" id="message"></textarea></div>
					<div class="col-md-3"><input name=" " type="submit" value="submit" onClick="validateContact();"></div>
                    </form>
                    
				</div>
			</div>

					
				</div>
				
				<div class="col-md-5">
					
					<div class="contact-get">
					<div class="main-title">
						<h2><span>GET IN</span> Touch</h2>
						<p>cursus lorem molestie vitae. Nulla vehicula, lacus ut suscipit fermentum, turpis felis ultricies.</p>
					 </div>
					
					<div class="get-in-touch">
					<div class="detail">
						<span><b>Phone:</b> 1.800.555.6789</span>
						<span><b>Email:</b> <a href="#.">support@medical.com</a></span>
						<span><b>Web:</b> <a href="#.">www.Medicalguide.com</a></span>
						<span><b>Address:</b> 12345 North Main Street, New York, NY</span>
					 </div>
					 
					 <div class="social-icons">
                        	<a href="#." class="fb"><i class="icon-euro"></i></a>
                            <a href="#." class="tw"><i class="icon-yen"></i></a>
                            <a href="#." class="gp"><i class="icon-google-plus"></i></a>
                            <a href="#." class="vimeo"><i class="icon-vimeo4"></i></a>
                     </div>
					 </div>
					 </div>
					 
				</div>
				
			</div>

			</div>
		</div>
		
   </div>
   @stop