@extends('frontend.master')

@section('content')
<div class="container">
  <div class="time-table-sec">
    <ul id="accordion2" class="accordion2">
        <li>
          <ul class="submenu time-table">
            <li class="tit"><h5>Working Time</h5></li>
            <li><span class="day">Monday - Friday</span> <span class="divider">-</span> <span class="time">8.00 - 16.00</span></li>
            <li><span class="day">Saturday</span> <span class="divider">-</span> <span class="time">9.15 - 17.00</span></li>
            <li><span class="day">Sunday</span> <span class="divider">-</span> <span class="time">9.45 - 15.00</span></li>
          </ul>
            <div class="link"><img class="time-tab" src="{{URL::to('frontend/images/timetable-menu.png')}}" alt="">
            </div>
        </li>
    </ul>
  </div>
</div>

    
  
   <!--Start Banner-->
   
   <div class="tp-banner-container">
    <div class="tp-banner" >
      <ul>  <!-- SLIDE  -->
    
  
  <li data-transition="fade" data-slotamount="7" data-masterspeed="500"  data-saveperformance="on"  data-title="Intro Slide">
    
    <img src="{{URL::to('frontend/images/slides/banner-img8.jpg')}}"  alt=""  data-bgposition="center top" data-bgfit="cover" data-bgrepeat="no-repeat">
    
    
    <div class="tp-caption customin customout rs-parallaxlevel-0"
      data-x="right"
      data-y="188" 
      data-customin="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0.75;scaleY:0.75;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"
      data-customout="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0.75;scaleY:0.75;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"
      data-speed="700"
      data-start="1200"
      data-easing="Power3.easeInOut"
      data-elementdelay="0.1"
      data-endelementdelay="0.1"
      style="z-index: 4;"><img class="transprint" src="{{URL::to('frontend/images/slides/transprint-bg.png')}}" alt="" >

    </div>
    
    
    <div class="tp-caption title-bold tp-resizeme rs-parallaxlevel-0 fade start"
      data-x="670"
      data-y="218" 
      data-speed="1000"
      data-start="1800"
      data-easing="Power3.easeInOut"
      data-splitin="none"
      data-splitout="none"
      data-elementdelay="0.1"
      data-endelementdelay="0.1"
      data-endspeed="300"
      style="z-index: 5; max-width: auto; max-height: auto; white-space: nowrap;">Medical care
    </div>
        
        <div class="tp-caption small-title tp-resizeme rs-parallaxlevel-0 fade start"
      data-x="670"
      data-y="255" 
      data-speed="1000"
      data-start="1800"
      data-easing="Power3.easeInOut"
      data-splitin="none"
      data-splitout="none"
      data-elementdelay="0.1"
      data-endelementdelay="0.1"
      data-endspeed="300"
      style="z-index: 5; max-width: auto; max-height: auto; white-space: nowrap;">Health care SOLUTION
    </div>

    <div class="tp-caption paragraph tp-resizeme rs-parallaxlevel-0 fade start"
      data-x="670"
      data-y="325" 
      data-speed="1000"
      data-start="1800"
      data-easing="Power3.easeInOut"
      data-splitin="none"
      data-splitout="none"
      data-elementdelay="0.05"
      data-endelementdelay="0.1"
      data-endspeed="300"
      style="z-index: 9; max-width: auto; max-height: auto; white-space: nowrap;"><div style="text-align:left;">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed <br/>do eiusmod tempor incididunt ut labore </div>
    </div>
    
    
    <div class="tp-caption banner-button tp-resizeme rs-parallaxlevel-0  fade start"
      data-x="670"
      data-y="402" 
      data-speed="1000"
      data-start="1800"
      data-easing="Power3.easeInOut"
      data-splitin="none"
      data-splitout="none"
      data-elementdelay="0.05"
      data-endelementdelay="0.1"
      data-endspeed="300"
      style="z-index: 9; max-width: auto; max-height: auto; white-space: nowrap; border-radius: 5px;"><div style="text-align:left; background:#525866; border-radius: 5px;">
      <a href="services2.html" class="read-more" style=" line-height: initial; color: #fff; text-transform: uppercase; font-weight: 500; padding: 12px 36px; display: inline-block; font-size: 18px; ">read more</a>
      </div>
    </div>
    
  </li>
  
  
  
  
  <li data-transition="fade" data-slotamount="7" data-masterspeed="500"  data-saveperformance="on"  data-title="Intro Slide">
    
    <img src="{{URL::to('frontend/images/slides/banner-img9.jpg')}}"  alt=""  data-bgposition="center top" data-bgfit="cover" data-bgrepeat="no-repeat">
    
    
    <div class="tp-caption customin customout rs-parallaxlevel-0"
      data-x="left"
      data-y="188" 
      data-customin="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0.75;scaleY:0.75;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"
      data-customout="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0.75;scaleY:0.75;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"
      data-speed="700"
      data-start="1200"
      data-easing="Power3.easeInOut"
      data-elementdelay="0.1"
      data-endelementdelay="0.1"
      style="z-index: 4;"><img style="border-bottom:solid 6px #ed1c24;" src="{{URL::to('frontend/images/slides/transprint-bg.png')}}" alt="" >

    </div>
    
    
    <div class="tp-caption title-bold tp-resizeme rs-parallaxlevel-0 fade start"
      data-x="30"
      data-y="218" 
      data-speed="1000"
      data-start="1800"
      data-easing="Power3.easeInOut"
      data-splitin="none"
      data-splitout="none"
      data-elementdelay="0.1"
      data-endelementdelay="0.1"
      data-endspeed="300"
      style="z-index: 5; max-width: auto; max-height: auto; white-space: nowrap;">Family Plans
    </div>
        
        <div class="tp-caption small-title tp-resizeme rs-parallaxlevel-0 fade start"
      data-x="30"
      data-y="255" 
      data-speed="1000"
      data-start="1800"
      data-easing="Power3.easeInOut"
      data-splitin="none"
      data-splitout="none"
      data-elementdelay="0.1"
      data-endelementdelay="0.1"
      data-endspeed="300"
      style="z-index: 5; max-width: auto; max-height: auto; white-space: nowrap;">affordable solutions
    </div>

    <div class="tp-caption paragraph tp-resizeme rs-parallaxlevel-0 fade start"
      data-x="30"
      data-y="325" 
      data-speed="1000"
      data-start="1800"
      data-easing="Power3.easeInOut"
      data-splitin="none"
      data-splitout="none"
      data-elementdelay="0.05"
      data-endelementdelay="0.1"
      data-endspeed="300"
      style="z-index: 9; max-width: auto; max-height: auto; white-space: nowrap;"><div style="text-align:left;">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed <br/>do eiusmod tempor incididunt ut labore </div>
    </div>
    
    
    <div class="tp-caption banner-button tp-resizeme rs-parallaxlevel-0  fade start"
      data-x="30"
      data-y="402" 
      data-speed="1000"
      data-start="1800"
      data-easing="Power3.easeInOut"
      data-splitin="none"
      data-splitout="none"
      data-elementdelay="0.05"
      data-endelementdelay="0.1"
      data-endspeed="300"
      style="z-index: 9; max-width: auto; max-height: auto; white-space: nowrap; border-radius: 5px;"><div style="text-align:left; background:#525866; border-radius: 5px;">
      <a href="patient-and-family.html" class="read-more" style=" line-height: initial; color: #fff; text-transform: uppercase; font-weight: 500; padding: 12px 36px; display: inline-block; font-size: 18px; ">read more</a>
      </div>
    </div>
    
  </li>
  
  
  
  
  <li data-transition="fade" data-slotamount="7" data-masterspeed="500"  data-saveperformance="on"  data-title="Intro Slide">
    
    <img src="{{URL::to('frontend/images/slides/banner-img10.jpg')}}"  alt=""  data-bgposition="center top" data-bgfit="cover" data-bgrepeat="no-repeat">
    
    
    <div class="tp-caption customin customout rs-parallaxlevel-0"
      data-x="right"
      data-y="188" 
      data-customin="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0.75;scaleY:0.75;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"
      data-customout="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0.75;scaleY:0.75;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"
      data-speed="700"
      data-start="1200"
      data-easing="Power3.easeInOut"
      data-elementdelay="0.1"
      data-endelementdelay="0.1"
      style="z-index: 4;"><img style="border-bottom:solid 6px #ed1c24;" src="{{URL::to('frontend/images/slides/transprint-bg.png')}}" alt="" >

    </div>
    
    
    <div class="tp-caption title-bold tp-resizeme rs-parallaxlevel-0 fade start"
      data-x="670"
      data-y="218" 
      data-speed="1000"
      data-start="1800"
      data-easing="Power3.easeInOut"
      data-splitin="none"
      data-splitout="none"
      data-elementdelay="0.1"
      data-endelementdelay="0.1"
      data-endspeed="300"
      style="z-index: 5; max-width: auto; max-height: auto; white-space: nowrap;">Research
    </div>
        
        <div class="tp-caption small-title tp-resizeme rs-parallaxlevel-0 fade start"
      data-x="670"
      data-y="255" 
      data-speed="1000"
      data-start="1800"
      data-easing="Power3.easeInOut"
      data-splitin="none"
      data-splitout="none"
      data-elementdelay="0.1"
      data-endelementdelay="0.1"
      data-endspeed="300"
      style="z-index: 5; max-width: auto; max-height: auto; white-space: nowrap;">cancer researchers honored
    </div>

    <div class="tp-caption paragraph tp-resizeme rs-parallaxlevel-0 fade start"
      data-x="670"
      data-y="325" 
      data-speed="1000"
      data-start="1800"
      data-easing="Power3.easeInOut"
      data-splitin="none"
      data-splitout="none"
      data-elementdelay="0.05"
      data-endelementdelay="0.1"
      data-endspeed="300"
      style="z-index: 9; max-width: auto; max-height: auto; white-space: nowrap;"><div style="text-align:left;">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed <br/>do eiusmod tempor incididunt ut labore </div>
    </div>
    
    
    <div class="tp-caption banner-button tp-resizeme rs-parallaxlevel-0  fade start"
      data-x="670"
      data-y="402" 
      data-speed="1000"
      data-start="1800"
      data-easing="Power3.easeInOut"
      data-splitin="none"
      data-splitout="none"
      data-elementdelay="0.05"
      data-endelementdelay="0.1"
      data-endspeed="300"
      style="z-index: 9; max-width: auto; max-height: auto; white-space: nowrap; border-radius: 5px;"><div style="text-align:left; background:#525866; border-radius: 5px;">
      <a href="procedures.html" class="read-more" style=" line-height: initial; color: #fff; text-transform: uppercase; font-weight: 500; padding: 12px 36px; display: inline-block; font-size: 18px; ">read more</a>
      </div>
    </div>
    
  </li>
  
  
</ul>
<div class="tp-bannertimer"></div>  </div>
</div>  
 <div class="content">
   
   <!--Start Services-->
   <div class="services-one">
          <div class="container">
              <div class="row">
                  <div class="col-md-6">
                      <div class="service-sec">
                          
                            <div class="icon">
                              <i class="icon-patient-bed"></i>
                            </div>
                            
                            <div class="detail">
                              <h5>Operation Theater</h5>
                                <p>If you need a doctor for to consectetuer Lorem ipsum dolor, consectetur adipiscing elit Lorem ipsum dolor, consectetur  Ut volutpat eros.</p>
                            </div>
                            
                        </div>
                    </div>
                    
                    <div class="col-md-6">
                      <div class="service-sec">
                          
                            <div class="icon">
                              <i class="icon-ambulanc"></i>
                            </div>
                            
                            <div class="detail">
                              <h5>Emergency Services</h5>
                                <p>If you need a doctor for to consectetuer Lorem ipsum dolor, consectetur adipiscing elit Lorem ipsum dolor, consectetur  Ut volutpat eros.</p>
                            </div>
                            
                        </div>
                    </div>
                    
                    
                    
                    <div class="col-md-6">
                      <div class="service-sec">
                          
                            <div class="icon">
                              <i class="icon-mortar-pestle"></i>
                            </div>
                            
                            <div class="detail">
                              <h5>Rehabilitation Center</h5>
                                <p>If you need a doctor for to consectetuer Lorem ipsum dolor, consectetur adipiscing elit Lorem ipsum dolor, consectetur  Ut volutpat eros.</p>
                            </div>
                            
                        </div>
                    </div>
                    
                    <div class="col-md-6">
                      <div class="service-sec">
                          
                            <div class="icon">
                              <i class="icon-doctor"></i>
                            </div>
                            
                            <div class="detail">
                              <h5>Qualified Doctors</h5>
                                <p>If you need a doctor for to consectetuer Lorem ipsum dolor, consectetur adipiscing elit Lorem ipsum dolor, consectetur  Ut volutpat eros.</p>
                            </div>
                            
                        </div>
                    </div>
                    
                    
                </div>
            </div>
        </div>
   <!--End Services-->
   
   
   <!--Start Welcome-->
    <div class="gallery">
     <div class="container">
      
        <div class="row">
        <div class="col-md-12">
            <div class="main-title">
                
                <h2><span>MyDoctionary</span>Popular Services</h2>
                <p>If you need a doctor for to consectetuer Lorem ipsum dolor, consectetur adipiscing elit. Ut volutpat eros  adipiscing nonummy.</p>
            </div>
        </div>
        </div>
     
    <div id="tabbed-nav">
                    <ul>
                        <li><a>Pharmacies</a></li>
                        <li><a>Laboratories</a></li>
                        <li><a>Hospitals</a></li>
                        <li><a>Doctors</a></li>
                    </ul>

                    
                        <div>
                        
                        <div>
                           
                           <div class="row">
                             <div class="main-gallery">

                              <div class="col-md-3">
                                <a class="gallery-sec fancybox photo-icon" href="{{route('order.medicine')}}" data-fancybox-group="gallery">    
                                              <div class="image-hover img-layer-slide-left-right">
                                  <img src="{{URL::to('frontend/images/Pharmacy/pharmacy1.jpg')}}" alt="">
                                                <div class="layer"> <i style="width: 50%;margin-left: -55px;height: 51px; font-size: 14px;">Order Medicine</i> </div>
                                                </div>
                                                </a>

                                                <a class="gallery-sec fancybox photo-icon" href="{{route('web.pharmacy_detail')}}" data-fancybox-group="gallery">  
                                                
                                  <div class="detail" style="margin-top: -50px;">
                                    <h6>SERVAID+</h6>
                                    <span>Cras porttitor mauris pulvinar</span>
                                  </div>
                                </a>
                                
                              </div>

                              <div class="col-md-3">
                                <a class="gallery-sec" href="{{route('order.medicine')}}" data-fancybox-group="gallery">    
                                              <div class="image-hover img-layer-slide-left-right">
                                  <img src="{{URL::to('frontend/images/Pharmacy/pharmacy2.jpg')}}" alt="">
                                                <div class="layer"> <i style="width: 50%;margin-left: -55px;height: 51px; font-size: 14px;">Order Medicine</i> </div>
                                                </div>
                                                </a>

                                                <a class="gallery-sec fancybox photo-icon" href="{{route('web.pharmacy_detail')}}" data-fancybox-group="gallery">  
                                                
                                  <div class="detail" style="margin-top: -50px;">
                                    <h6>FAZAL DIN'S</h6>
                                    <span>Cras porttitor mauris pulvinar</span>
                                  </div>
                                </a>
                                
                              </div>

                              <div class="col-md-3">
                                <a class="gallery-sec fancybox photo-icon" href="{{route('order.medicine')}}" data-fancybox-group="gallery">    
                                              <div class="image-hover img-layer-slide-left-right">
                                  <img src="{{URL::to('frontend/images/Pharmacy/pharmacy3.jpg')}}" alt="">
                                                <div class="layer"> <i style="width: 50%;margin-left: -55px;height: 51px; font-size: 14px;">Order Medicine</i> </div>
                                                </div>
                                                </a>

                                                <a class="gallery-sec fancybox photo-icon" href="{{route('web.pharmacy_detail')}}" data-fancybox-group="gallery">  
                                                
                                  <div class="detail" style="margin-top: -50px;">
                                    <h6>Med-X PHARMACY</h6>
                                    <span>Cras porttitor mauris pulvinar</span>
                                  </div>
                                </a>
                                
                              </div>

                              <div class="col-md-3">
                                <a class="gallery-sec fancybox photo-icon" href="{{route('order.medicine')}}" data-fancybox-group="gallery">    
                                  <div class="image-hover img-layer-slide-left-right">
                                  <img src="{{URL::to('frontend/images/Pharmacy/pharmacy4.jpg')}}" alt="">
                                  <div class="layer"> <i style="width: 50%;margin-left: -55px;height: 51px; font-size: 14px;">Order Medicine</i> </div>
                                    </div>
                                    </a>

                                    <a class="gallery-sec fancybox photo-icon" href="{{route('web.pharmacy_detail')}}" data-fancybox-group="gallery">  
                                                
                                    <div class="detail" style="margin-top: -50px;">
                                     <h6>LIFE CARE PHARMACY</h6>
                                     <span>Cras porttitor mauris pulvinar</span>
                                   </div>
                                </a>
                                
                              </div>

                             </div>
                           </div>
                           <div class="row">
                             <div class="main-gallery">

                              <div class="col-md-3">
                                <a class="gallery-sec fancybox photo-icon" href="{{route('order.medicine')}}" data-fancybox-group="gallery">    
                                              <div class="image-hover img-layer-slide-left-right">
                                  <img src="{{URL::to('frontend/images/Pharmacy/pharmacy5.jpg')}}" alt="">
                                                <div class="layer"> <i style="width: 50%;margin-left: -55px;height: 51px; font-size: 14px;">Order Medicine</i> </div>
                                                </div>
                                                </a>

                                                <a class="gallery-sec fancybox photo-icon" href="{{route('web.pharmacy_detail')}}" data-fancybox-group="gallery">  
                                                
                                  <div class="detail" style="margin-top: -50px;">
                                    <h6>CARE PHARMACY</h6>
                                    <span>Cras porttitor mauris pulvinar</span>
                                  </div>
                                </a>
                                
                              </div>

                              <div class="col-md-3">
                                <a class="gallery-sec" href="{{route('order.medicine')}}" data-fancybox-group="gallery">    
                                              <div class="image-hover img-layer-slide-left-right">
                                  <img src="{{URL::to('frontend/images/Pharmacy/pharmacy6.jpg')}}" alt="">
                                                <div class="layer"> <i style="width: 50%;margin-left: -55px;height: 51px; font-size: 14px;">Order Medicine</i> </div>
                                                </div>
                                                </a>

                                                <a class="gallery-sec fancybox photo-icon" href="{{route('web.pharmacy_detail')}}" data-fancybox-group="gallery">  
                                                
                                  <div class="detail" style="margin-top: -50px;">
                                    <h6>KHALID PHARMACY</h6>
                                    <span>Cras porttitor mauris pulvinar</span>
                                  </div>
                                </a>
                                
                              </div>

                              <div class="col-md-3">
                                <a class="gallery-sec fancybox photo-icon" href="{{route('order.medicine')}}" data-fancybox-group="gallery">    
                                              <div class="image-hover img-layer-slide-left-right">
                                  <img src="{{URL::to('frontend/images/Pharmacy/pharmacy7.jpg')}}" alt="">
                                                <div class="layer"> <i style="width: 50%;margin-left: -55px;height: 51px; font-size: 14px;">Order Medicine</i> </div>
                                                </div>
                                                </a>

                                                <a class="gallery-sec fancybox photo-icon" href="{{route('web.pharmacy_detail')}}" data-fancybox-group="gallery">  
                                                
                                  <div class="detail" style="margin-top: -50px;">
                                    <h6>HEALTH MART</h6>
                                    <span>Cras porttitor mauris pulvinar</span>
                                  </div>
                                </a>
                                
                              </div>

                              <div class="col-md-3">
                                <a class="gallery-sec fancybox photo-icon" href="{{route('order.medicine')}}" data-fancybox-group="gallery">    
                                  <div class="image-hover img-layer-slide-left-right">
                                  <img src="{{URL::to('frontend/images/Pharmacy/pharmacy8.jpg')}}" alt="">
                                  <div class="layer"> <i style="width: 50%;margin-left: -55px;height: 51px; font-size: 14px;">Order Medicine</i> </div>
                                    </div>
                                    </a>

                                    <a class="gallery-sec fancybox photo-icon" href="{{route('web.pharmacy_detail')}}" data-fancybox-group="gallery">  
                                                
                                    <div class="detail" style="margin-top: -50px;">
                                     <h6>Pakistan PHARMACITS</h6>
                                     <span>Cras porttitor mauris pulvinar</span>
                                   </div>
                                </a>
                                
                              </div>

                             </div>
                           </div>
                           
                            
                        </div>
                        
                        <div>
                           
                          <div class="row">
                             <div class="main-gallery">

                              <div class="col-md-3">
                                <a class="gallery-sec fancybox photo-icon" href="{{route('lab.book_appointment')}}" data-fancybox-group="gallery">    
                                              <div class="image-hover img-layer-slide-left-right">
                                  <img src="{{URL::to('frontend/images/labs/lab1.jpg')}}" alt="">
                                                <div class="layer"> <i style="width: 50%;margin-left: -55px;height: 51px; font-size: 13px;">Book Appointment</i> </div>
                                                </div>
                                                </a>

                                                <a class="gallery-sec fancybox photo-icon" href="{{route('web.lab_detail')}}" data-fancybox-group="gallery">  
                                                
                                  <div class="detail" style="margin-top: -50px;">
                                    <h6>CHUGHTAI LAB</h6>
                                    <span>Cras porttitor mauris pulvinar</span>
                                  </div>
                                </a>
                                
                              </div>

                              <div class="col-md-3">
                                <a class="gallery-sec" href="{{route('lab.book_appointment')}}" data-fancybox-group="gallery">    
                                              <div class="image-hover img-layer-slide-left-right">
                                  <img src="{{URL::to('frontend/images/labs/lab2.jpg')}}" alt="">
                                                <div class="layer"> <i style="width: 50%;margin-left: -55px;height: 51px; font-size: 13px;">Book Appointment</i> </div>
                                                </div>
                                                </a>

                                                <a class="gallery-sec fancybox photo-icon" href="{{route('web.lab_detail')}}" data-fancybox-group="gallery">  
                                                
                                  <div class="detail" style="margin-top: -50px;">
                                    <h6>CITI LAB</h6>
                                    <span>Cras porttitor mauris pulvinar</span>
                                  </div>
                                </a>
                                
                              </div>

                              <div class="col-md-3">
                                <a class="gallery-sec fancybox photo-icon"href="{{route('lab.book_appointment')}}" data-fancybox-group="gallery">    
                                              <div class="image-hover img-layer-slide-left-right">
                                  <img src="{{URL::to('frontend/images/labs/lab3.jpg')}}" alt="">
                                                <div class="layer"> <i style="width: 50%;margin-left: -55px;height: 51px; font-size: 13px;">Book Appointment</i> </div>
                                                </div>
                                                </a>

                                                <a class="gallery-sec fancybox photo-icon" href="{{route('web.lab_detail')}}" data-fancybox-group="gallery">  
                                                
                                  <div class="detail" style="margin-top: -50px;">
                                    <h6>BISMIL LAB</h6>
                                    <span>Cras porttitor mauris pulvinar</span>
                                  </div>
                                </a>
                                
                              </div>

                              <div class="col-md-3">
                                <a class="gallery-sec fancybox photo-icon" href="{{route('lab.book_appointment')}}" data-fancybox-group="gallery">    
                                  <div class="image-hover img-layer-slide-left-right">
                                  <img src="{{URL::to('frontend/images/labs/lab4.jpg')}}" alt="">
                                  <div class="layer"> <i style="width: 50%;margin-left: -55px;height: 51px; font-size: 13px;">Book Appointment</i> </div>
                                    </div>
                                    </a>

                                    <a class="gallery-sec fancybox photo-icon" href="{{route('web.lab_detail')}}" data-fancybox-group="gallery">  
                                                
                                    <div class="detail" style="margin-top: -50px;">
                                     <h6>LAHORE PCR LAB (PVT)</h6>
                                     <span>Cras porttitor mauris pulvinar</span>
                                   </div>
                                </a>
                                
                              </div>

                             </div>
                           </div>
                           <div class="row">
                             <div class="main-gallery">

                              <div class="col-md-3">
                                <a class="gallery-sec fancybox photo-icon" href="{{route('lab.book_appointment')}}" data-fancybox-group="gallery">    
                                              <div class="image-hover img-layer-slide-left-right">
                                  <img src="{{URL::to('frontend/images/labs/lab5.jpg')}}" alt="">
                                                <div class="layer"> <i style="width: 50%;margin-left: -55px;height: 51px; font-size: 13px;">Book Appointment</i> </div>
                                                </div>
                                                </a>

                                                <a class="gallery-sec fancybox photo-icon" href="{{route('web.lab_detail')}}" data-fancybox-group="gallery">  
                                                
                                  <div class="detail" style="margin-top: -50px;">
                                    <h6>DDLC</h6>
                                    <span>Cras porttitor mauris pulvinar</span>
                                  </div>
                                </a>
                                
                              </div>

                              <div class="col-md-3">
                                <a class="gallery-sec" href="{{route('lab.book_appointment')}}" data-fancybox-group="gallery">    
                                              <div class="image-hover img-layer-slide-left-right">
                                  <img src="{{URL::to('frontend/images/labs/lab6.jpg')}}" alt="">
                                                <div class="layer"> <i style="width: 50%;margin-left: -55px;height: 51px; font-size: 13px;">Book Appointment</i> </div>
                                                </div>
                                                </a>

                                                <a class="gallery-sec fancybox photo-icon" href="{{route('web.lab_detail')}}" data-fancybox-group="gallery">  
                                                
                                  <div class="detail" style="margin-top: -50px;">
                                    <h6>PUNJAB HEALTH CENTER</h6>
                                    <span>Cras porttitor mauris pulvinar</span>
                                  </div>
                                </a>
                                
                              </div>

                              <div class="col-md-3">
                                <a class="gallery-sec fancybox photo-icon" href="{{route('lab.book_appointment')}}" data-fancybox-group="gallery">    
                                              <div class="image-hover img-layer-slide-left-right">
                                  <img src="{{URL::to('frontend/images/labs/lab7.jpg')}}" alt="">
                                                <div class="layer"> <i style="width: 50%;margin-left: -55px;height: 51px; font-size: 13px;">Book Appointment</i> </div>
                                                </div>
                                                </a>

                                                <a class="gallery-sec fancybox photo-icon" href="{{route('web.lab_detail')}}" data-fancybox-group="gallery">  
                                                
                                  <div class="detail" style="margin-top: -50px;">
                                    <h6>AGHA KHAN LAB</h6>
                                    <span>Cras porttitor mauris pulvinar</span>
                                  </div>
                                </a>
                                
                              </div>

                              <div class="col-md-3">
                                <a class="gallery-sec fancybox photo-icon" href="{{route('lab.book_appointment')}}" data-fancybox-group="gallery">    
                                  <div class="image-hover img-layer-slide-left-right">
                                  <img src="{{URL::to('frontend/images/labs/lab8.jpg')}}" alt="">
                                  <div class="layer"> <i style="width: 50%;margin-left: -55px;height: 51px; font-size: 13px;">Book Appointment</i> </div>
                                    </div>
                                    </a>

                                    <a class="gallery-sec fancybox photo-icon" href="{{route('web.lab_detail')}}" data-fancybox-group="gallery">  
                                                
                                    <div class="detail" style="margin-top: -50px;">
                                     <h6>LAB CARE</h6>
                                     <span>Cras porttitor mauris pulvinar</span>
                                   </div>
                                </a>
                                
                              </div>

                             </div>
                           </div>
                           
                            
                        </div>
                        
                        <div>
                           
                           <div class="row">
                             <div class="main-gallery">

                              <div class="col-md-3">
                                <a class="gallery-sec fancybox photo-icon" href="{{route('hospital.book_appointment')}}" data-fancybox-group="gallery">    
                                              <div class="image-hover img-layer-slide-left-right">
                                  <img src="{{URL::to('frontend/images/hospital/hospital1.jpg')}}" alt="">
                                                <div class="layer"> <i style="width: 50%;margin-left: -55px;height: 51px; font-size: 13px;">Book Appointment</i> </div>
                                                </div>
                                                </a>

                                                <a class="gallery-sec fancybox photo-icon" href="{{route('web.hospital_detail')}}" data-fancybox-group="gallery">  
                                                
                                  <div class="detail" style="margin-top: -50px;">
                                    <h6>SHEIKH ZAYED HOSPITAL</h6>
                                    <span>Cras porttitor mauris pulvinar</span>
                                  </div>
                                </a>
                                
                              </div>

                              <div class="col-md-3">
                                <a class="gallery-sec" href="{{route('hospital.book_appointment')}}" data-fancybox-group="gallery">    
                                              <div class="image-hover img-layer-slide-left-right">
                                  <img src="{{URL::to('frontend/images/hospital/hospital2.jpg')}}" alt="">
                                                <div class="layer"> <i style="width: 50%;margin-left: -55px;height: 51px; font-size: 13px;">Book Appointment</i> </div>
                                                </div>
                                                </a>

                                                <a class="gallery-sec fancybox photo-icon" href="{{route('web.hospital_detail')}}" data-fancybox-group="gallery">  
                                                
                                  <div class="detail" style="margin-top: -50px;">
                                    <h6>JINNAH HOSPITAL LAHORE</h6>
                                    <span>Cras porttitor mauris pulvinar</span>
                                  </div>
                                </a>
                                
                              </div>

                              <div class="col-md-3">
                                <a class="gallery-sec fancybox photo-icon" href="{{route('hospital.book_appointment')}}" data-fancybox-group="gallery">    
                                              <div class="image-hover img-layer-slide-left-right">
                                  <img src="{{URL::to('frontend/images/hospital/hospital3.jpg')}}" alt="">
                                                <div class="layer"> <i style="width: 50%;margin-left: -55px;height: 51px; font-size: 13px;">Book Appointment</i> </div>
                                                </div>
                                                </a>

                                                <a class="gallery-sec fancybox photo-icon" href="{{route('web.hospital_detail')}}" data-fancybox-group="gallery">  
                                                
                                  <div class="detail" style="margin-top: -50px;">
                                    <h6>FATIMA MEMORIAL HOSPITAL</h6>
                                    <span>Cras porttitor mauris pulvinar</span>
                                  </div>
                                </a>
                                
                              </div>

                              <div class="col-md-3">
                                <a class="gallery-sec fancybox photo-icon" href="{{route('hospital.book_appointment')}}" data-fancybox-group="gallery">    
                                  <div class="image-hover img-layer-slide-left-right">
                                  <img src="{{URL::to('frontend/images/hospital/hospital4.jpg')}}" alt="">
                                  <div class="layer"> <i style="width: 50%;margin-left: -55px;height: 51px; font-size: 13px;">Book Appointment</i> </div>
                                    </div>
                                    </a>

                                    <a class="gallery-sec fancybox photo-icon" href="{{route('web.hospital_detail')}}" data-fancybox-group="gallery">  
                                                
                                    <div class="detail" style="margin-top: -50px;">
                                     <h6>SIR GANGARAM HOSPITAL</h6>
                                     <span>Cras porttitor mauris pulvinar</span>
                                   </div>
                                </a>
                                
                              </div>

                             </div>
                           </div>
                           <div class="row">
                             <div class="main-gallery">

                              <div class="col-md-3">
                                <a class="gallery-sec fancybox photo-icon" href="{{route('hospital.book_appointment')}}" data-fancybox-group="gallery">    
                                              <div class="image-hover img-layer-slide-left-right">
                                  <img src="{{URL::to('frontend/images/hospital/hospital5.jpg')}}" alt="">
                                                <div class="layer"> <i style="width: 50%;margin-left: -55px;height: 51px; font-size: 13px;">Book Appointment</i> </div>
                                                </div>
                                                </a>

                                                <a class="gallery-sec fancybox photo-icon" href="{{route('web.hospital_detail')}}" data-fancybox-group="gallery">  
                                                
                                  <div class="detail" style="margin-top: -50px;">
                                    <h6>LAHORE GENERAL HOSPITAL</h6>
                                    <span>Cras porttitor mauris pulvinar</span>
                                  </div>
                                </a>
                                
                              </div>

                              <div class="col-md-3">
                                <a class="gallery-sec" href="{{route('hospital.book_appointment')}}" data-fancybox-group="gallery">    
                                              <div class="image-hover img-layer-slide-left-right">
                                  <img src="{{URL::to('frontend/images/hospital/hospital6.jpg')}}" alt="">
                                                <div class="layer"> <i style="width: 50%;margin-left: -55px;height: 51px; font-size: 13px;">Book Appointment</i> </div>
                                                </div>
                                                </a>

                                                <a class="gallery-sec fancybox photo-icon" href="{{route('web.hospital_detail')}}" data-fancybox-group="gallery">  
                                                
                                  <div class="detail" style="margin-top: -50px;">
                                    <h6>MAYO HOSPITAL LAHORE</h6>
                                    <span>Cras porttitor mauris pulvinar</span>
                                  </div>
                                </a>
                                
                              </div>

                              <div class="col-md-3">
                                <a class="gallery-sec fancybox photo-icon" href="{{route('hospital.book_appointment')}}" data-fancybox-group="gallery">    
                                              <div class="image-hover img-layer-slide-left-right">
                                  <img src="{{URL::to('frontend/images/hospital/hospital7.jpg')}}" alt="">
                                                <div class="layer"> <i style="width: 50%;margin-left: -55px;height: 51px; font-size: 13px;">Book Appointment</i> </div>
                                                </div>
                                                </a>

                                                <a class="gallery-sec fancybox photo-icon" href="{{route('web.hospital_detail')}}" data-fancybox-group="gallery">  
                                                
                                  <div class="detail" style="margin-top: -50px;">
                                    <h6>NATIONAL HOSPITAL </h6>
                                    <span>Cras porttitor mauris pulvinar</span>
                                  </div>
                                </a>
                                
                              </div>

                              <div class="col-md-3">
                                <a class="gallery-sec fancybox photo-icon" href="{{route('hospital.book_appointment')}}" data-fancybox-group="gallery">    
                                  <div class="image-hover img-layer-slide-left-right">
                                  <img src="{{URL::to('frontend/images/hospital/hospital8.jpg')}}" alt="">
                                  <div class="layer"> <i style="width: 50%;margin-left: -55px;height: 51px; font-size: 13px;">Book Appointment</i> </div>
                                    </div>
                                    </a>

                                    <a class="gallery-sec fancybox photo-icon" href="{{route('web.hospital_detail')}}" data-fancybox-group="gallery">  
                                                
                                    <div class="detail" style="margin-top: -50px;">
                                     <h6>SURGIED HOSPITAL</h6>
                                     <span>Cras porttitor mauris pulvinar</span>
                                   </div>
                                </a>
                                
                              </div>

                             </div>
                           </div>
                           
                           
                            
                        </div>
                        
                        <div>
                           
                           <div class="row">
                             <div class="main-gallery">

                              <div class="col-md-3">
                                <a class="gallery-sec fancybox photo-icon" href="{{route('doctor.book_appointment')}}" data-fancybox-group="gallery">    
                                              <div class="image-hover img-layer-slide-left-right">
                                  <img src="{{URL::to('frontend/images/doctor/doctor1.jpg')}}" alt="">
                                                <div class="layer"> <i style="width: 50%;margin-left: -55px;height: 51px; font-size: 13px;">Book Appointment</i> </div>
                                                </div>
                                                </a>

                                                <a class="gallery-sec fancybox photo-icon" href="{{route('web.doctor_detail')}}" data-fancybox-group="gallery">  
                                                
                                  <div class="detail" style="margin-top: -50px;">
                                    <h6>Dr. MUSKAN</h6>
                                    <span>Cras porttitor mauris pulvinar</span>
                                  </div>
                                </a>
                                
                              </div>

                              <div class="col-md-3">
                                <a class="gallery-sec" href="{{route('doctor.book_appointment')}}" data-fancybox-group="gallery">    
                                              <div class="image-hover img-layer-slide-left-right">
                                  <img src="{{URL::to('frontend/images/doctor/doctor2.jpg')}}" alt="">
                                                <div class="layer"> <i style="width: 50%;margin-left: -55px;height: 51px; font-size: 13px;">Book Appointment</i> </div>
                                                </div>
                                                </a>

                                                <a class="gallery-sec fancybox photo-icon" href="{{route('web.doctor_detail')}}" data-fancybox-group="gallery">  
                                                
                                  <div class="detail" style="margin-top: -50px;">
                                    <h6>Dr. ALISHBA</h6>
                                    <span>Cras porttitor mauris pulvinar</span>
                                  </div>
                                </a>
                                
                              </div>

                              <div class="col-md-3">
                                <a class="gallery-sec fancybox photo-icon" href="{{route('doctor.book_appointment')}}" data-fancybox-group="gallery">    
                                              <div class="image-hover img-layer-slide-left-right">
                                  <img src="{{URL::to('frontend/images/doctor/doctor3.jpg')}}" alt="">
                                                <div class="layer"> <i style="width: 50%;margin-left: -55px;height: 51px; font-size: 13px;">Book Appointment</i> </div>
                                                </div>
                                                </a>

                                                <a class="gallery-sec fancybox photo-icon" href="{{route('web.doctor_detail')}}" data-fancybox-group="gallery">  
                                                
                                  <div class="detail" style="margin-top: -50px;">
                                    <h6>Dr. FATIMA</h6>
                                    <span>Cras porttitor mauris pulvinar</span>
                                  </div>
                                </a>
                                
                              </div>

                              <div class="col-md-3">
                                <a class="gallery-sec fancybox photo-icon" href="{{route('doctor.book_appointment')}}" data-fancybox-group="gallery">    
                                  <div class="image-hover img-layer-slide-left-right">
                                  <img src="{{URL::to('frontend/images/doctor/doctor4.jpg')}}" alt="">
                                  <div class="layer"> <i style="width: 50%;margin-left: -55px;height: 51px; font-size: 13px;">Book Appointment</i> </div>
                                    </div>
                                    </a>

                                    <a class="gallery-sec fancybox photo-icon" href="{{route('web.doctor_detail')}}" data-fancybox-group="gallery">  
                                                
                                    <div class="detail" style="margin-top: -50px;">
                                     <h6>Dr. FAROOQ AZAM</h6>
                                     <span>Cras porttitor mauris pulvinar</span>
                                   </div>
                                </a>
                                
                              </div>

                             </div>
                           </div>
                           <div class="row">
                             <div class="main-gallery">

                              <div class="col-md-3">
                                <a class="gallery-sec fancybox photo-icon" href="{{route('doctor.book_appointment')}}" data-fancybox-group="gallery">    
                                              <div class="image-hover img-layer-slide-left-right">
                                  <img src="{{URL::to('frontend/images/doctor/doctor5.jpg')}}" alt="">
                                                <div class="layer"> <i style="width: 50%;margin-left: -55px;height: 51px; font-size: 13px;">Book Appointment</i> </div>
                                                </div>
                                                </a>

                                                <a class="gallery-sec fancybox photo-icon" href="{{route('web.doctor_detail')}}" data-fancybox-group="gallery">  
                                                
                                  <div class="detail" style="margin-top: -50px;">
                                    <h6>Dr. AYESHA</h6>
                                    <span>Cras porttitor mauris pulvinar</span>
                                  </div>
                                </a>
                                
                              </div>

                              <div class="col-md-3">
                                <a class="gallery-sec" href="{{route('doctor.book_appointment')}}" data-fancybox-group="gallery">    
                                              <div class="image-hover img-layer-slide-left-right">
                                  <img src="{{URL::to('frontend/images/doctor/doctor6.jpg')}}" alt="">
                                                <div class="layer"> <i style="width: 50%;margin-left: -55px;height: 51px; font-size: 13px;">Book Appointment</i> </div>
                                                </div>
                                                </a>

                                                <a class="gallery-sec fancybox photo-icon" href="{{route('web.doctor_detail')}}" data-fancybox-group="gallery">  
                                                
                                  <div class="detail" style="margin-top: -50px;">
                                    <h6>Dr. AHSAN BILAL</h6>
                                    <span>Cras porttitor mauris pulvinar</span>
                                  </div>
                                </a>
                                
                              </div>

                              <div class="col-md-3">
                                <a class="gallery-sec fancybox photo-icon" href="{{route('doctor.book_appointment')}}" data-fancybox-group="gallery">    
                                              <div class="image-hover img-layer-slide-left-right">
                                  <img src="{{URL::to('frontend/images/doctor/doctor7.jpg')}}" alt="">
                                                <div class="layer"> <i style="width: 50%;margin-left: -55px;height: 51px; font-size: 13px;">Book Appointment</i> </div>
                                                </div>
                                                </a>

                                                <a class="gallery-sec fancybox photo-icon" href="{{route('web.doctor_detail')}}" data-fancybox-group="gallery">  
                                                
                                  <div class="detail" style="margin-top: -50px;">
                                    <h6>Dr. SABEEN</h6>
                                    <span>Cras porttitor mauris pulvinar</span>
                                  </div>
                                </a>
                                
                              </div>

                              <div class="col-md-3">
                                <a class="gallery-sec fancybox photo-icon" href="{{route('doctor.book_appointment')}}" data-fancybox-group="gallery">    
                                  <div class="image-hover img-layer-slide-left-right">
                                  <img src="{{URL::to('frontend/images/doctor/doctor8.jpg')}}" alt="">
                                  <div class="layer"> <i style="width: 50%;margin-left: -55px;height: 51px; font-size: 13px;">Book Appointment</i> </div>
                                    </div>
                                    </a>

                                    <a class="gallery-sec fancybox photo-icon" href="{{route('web.doctor_detail')}}" data-fancybox-group="gallery">  
                                                
                                    <div class="detail" style="margin-top: -50px;">
                                     <h6>Dr. RASHNA JAVED</h6>
                                     <span>Cras porttitor mauris pulvinar</span>
                                   </div>
                                </a>
                                
                              </div>

                             </div>
                           </div>
                           
                            
                        </div>
                        
                        </div>
                        
        </div>
               
                
     </div>
   </div>
   <!--End Welcome-->
   <!--Start Latest News-->
   <div class="latest-news dark-back">
      <div class="container">
          
            <div class="row">
          <div class="col-md-12">
            <div class="main-title">
                <h2><span>Latest News from</span> Medical guide</h2>
                <p>If you need a doctor for to consectetuer Lorem ipsum dolor, consectetur adipiscing elit. Ut volutpat eros  adipiscing nonummy.</p>
            </div>
            </div>
            </div>
            
           
           
           
            <div id="latest-news">
        <div class="container">
          <div class="row">
            <div class="span12">

              <div id="owl-demo" class="owl-carousel">

                <div class="post item">
                      <img class="lazyOwl" src="{{URL::to('frontend/images/news-img1.jpg')}}" alt="">
                        <div class="detail">
                          <img src="{{URL::to('frontend/images/news-Dr.1.jpg')}}" alt="">
                            <h4><a href="news-detail.html">Center for Medical</a></h4>
                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit eros...</p>
                            <span><i class="icon-clock3"></i> Apr 22, 2016</span>
                            <span class="comment"><a href="news-detail.html"><i class="icon-icons206"></i> 5 Comments</a></span>
                        </div>
                    </div>
                <div class="post item">
                    <img class="lazyOwl" src="{{URL::to('frontend/images/news-img2.jpg')}}" alt="">
                    <div class="detail">
                        <img src="{{URL::to('frontend/images/news-Dr.2.jpg')}}" alt="">
                        <h4><a href="news-detail.html">Laboratory Tests</a></h4>
                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit eros...</p>
                        <span><i class="icon-clock3"></i> Apr 09, 2016</span>
                        <span class="comment"><a href="news-detail.html"><i class="icon-icons206"></i> 3 Comments</a></span>
                    </div>
                </div>
                
                <div class="post item">
                    <img class="lazyOwl" src="{{URL::to('frontend/images/news-img3.jpg')}}" alt="">
                    <div class="detail">
                        <img src="{{URL::to('frontend/images/news-Dr.1.jpg')}}" alt="">
                        <h4><a href="news-detail.html">Research Center</a></h4>
                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit eros...</p>
                        <span><i class="icon-clock3"></i> Mar 28, 2016</span>
                        <span class="comment"><a href="news-detail.html"><i class="icon-icons206"></i> 0 Comments</a></span>
                    </div>
                </div>
                
                <div class="post item">
                    <img class="lazyOwl" src="{{URL::to('frontend/images/news-img4.jpg')}}" alt="">
                    <div class="detail">
                        <img src="{{URL::to('frontend/images/news-Dr.2.jpg')}}" alt="">
                        <h4><a href="news-detail.html">Child Birth</a></h4>
                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit eros...</p>
                        <span><i class="icon-clock3"></i> Mar 15, 2016</span>
                        <span class="comment"><a href="news-detail.html"><i class="icon-icons206"></i> 0 Comments</a></span>
                    </div>
                </div>
                
                <div class="post item">
                    <img class="lazyOwl" src="{{URL::to('frontend/images/news-img5.jpg')}}" alt="">
                    <div class="detail">
                        <img src="{{URL::to('frontend/images/news-Dr.2.jpg')}}" alt="">
                        <h4><a href="news-detail.html">Special Treatment</a></h4>
                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit eros...</p>
                        <span><i class="icon-clock3"></i> Mar 04, 2016</span>
                        <span class="comment"><a href="news-detail.html"><i class="icon-icons206"></i> 11 Comments</a></span>
                    </div>
                </div>    

              </div>

            </div>
          </div>
        </div>

    </div>
    
    
            
        </div>
   </div>
   <!--End Latest News-->
   <!--Start Doctor Quote-->
   <div class="Dr.-quote">
      <div class="container">
          <div class="row">
              <div class="col-md-12">
                  <span class="quote">"Meeting the challenges of an ever-changing healthcare environment."</span>
                    <span class="name">- Dr.. Jonathan Gobi</span>
                </div>
            </div>
        </div>
   </div>
   <!--End Doctor Quote-->
   
   
   
   <!--Start Specialists-->
   <div class="meet-specialists">
      <div class="container">
          
            
      <div class="row">
          <div class="col-md-12">
            <div class="main-title">
                <h2><span>Meet Our</span> Specialists</h2>
                <p>If you need a doctor for to consectetuer Lorem ipsum dolor, consectetur adipiscing elit. Ut volutpat eros  adipiscing nonummy.</p>
            </div>
            </div>
            </div>
            
             <div id="demo">
              <div class="container">
               <div class="row">
                 <div class="span12">

                   <div id="owl-demo4" class="owl-carousel">
        
                     <div class="post item">
                  
                  <div class="gallery-sec">   
                                  <div class="image-hover img-layer-slide-left-right">
                      <img src="{{URL::to('frontend/images/team-member1.jpg')}}" alt="">
                        <div class="layer">
                          <a href="{{route('doctor.book_appointment')}}" class="btn btn-seconDr.y">Book Appointment</a>
                         </div>
                                    </div>
                                </div>
                    
                            <div class="detail" style="text-align: center;">
                              <h6><a href="{{route('web.doctor_detail')}}">Dr.. AnDr.ew Bert</a></h6>
                                <span>Outpatient Surgery</span>
                                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec nec eros eget nisl fringilla commodo.</p>
                                <a href="{{route('web.doctor_detail')}}">- View Profile</a>
                            </div>
                            
                        </div>
                <div class="post item">
                  
                  <div class="gallery-sec">   
                                  <div class="image-hover img-layer-slide-left-right">
                      <img src="{{URL::to('frontend/images/team-member2.jpg')}}" alt="">
                           <div class="layer">
                          <a href="{{route('doctor.book_appointment')}}" class="btn btn-seconDr.y">Book Appointment</a>
                         </div>
                                    </div>
                                </div>
                    
                            <div class="detail" style="text-align: center;">
                              <h6><a href="{{route('web.doctor_detail')}}">Dr.. Sara Stefon</a></h6>
                                <span>Cardiologist</span>
                                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec nec eros eget nisl fringilla commodo.</p>
                                <a href="{{route('web.doctor_detail')}}">- View Profile</a>
                            </div>
                            
                        </div>
            <div class="post item">
                  
                <div class="gallery-sec">   
                                <div class="image-hover img-layer-slide-left-right">
                    <img src="{{URL::to('frontend/images/team-member3.jpg')}}" alt="">
                    <div class="layer">
                          <a href="{{route('doctor.book_appointment')}}" class="btn btn-seconDr.y">Book Appointment</a>
                         </div>
                                  </div>
                              </div>
                  
                          <div class="detail" style="text-align: center;">
                            <h6><a href="{{route('web.doctor_detail')}}">Dr.. Wahab Apple</a></h6>
                              <span>Heart Specialist</span>
                              <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec nec eros eget nisl fringilla commodo.</p>
                              <a href="{{route('web.doctor_detail')}}">- View Profile</a>
                          </div>
                          
                      </div>
                
        <div class="post item">
                  
          <div class="gallery-sec">   
                          <div class="image-hover img-layer-slide-left-right">
              <img src="{{URL::to('frontend/images/team-member4.jpg')}}" alt="">
                   <div class="layer">
                          <a href="{{route('doctor.book_appointment')}}" class="btn btn-seconDr.y">Book Appointment</a>
                         </div>
                            </div>
                        </div>
            
                    <div class="detail" style="text-align: center;">
                      <h6><a href="{{route('web.doctor_detail')}}">Dr.. Mecan smith</a></h6>
                        <span>Heart Specialist</span>
                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec nec eros eget nisl fringilla commodo.</p>
                        <a href="{{route('web.doctor_detail')}}">- View Profile</a>
                    </div>
                    
                </div>
        
        <div class="post item">
            
            <div class="gallery-sec">   
                          <div class="image-hover img-layer-slide-left-right">
              <img src="{{URL::to('frontend/images/team-member5.jpg')}}" alt="">
                    <div class="layer">
                          <a href="{{route('doctor.book_appointment')}}" class="btn btn-seconDr.y">Book Appointment</a>
                         </div>
                            </div>
                        </div>
            
                    <div class="detail" style="text-align: center;">
                      <h6><a href="{{route('web.doctor_detail')}}">Dr.. Jack Bravo</a></h6>
                        <span>Heart Specialist</span>
                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec nec eros eget nisl fringilla commodo.</p>
                        <a href="{{route('web.doctor_detail')}}">- View Profile</a>
                    </div>
                    
                </div>
        
                </div>
        </div>
            </div>
            </div>
      </div>
        </div>
   </div>
   <!--End Specialists-->
   
   
   
   
   
   
   
   
   
   <!--Start Testimonials-->
   <div class="patients-testi dark-testi">
    <div class="container">
        
        <div class="row">
        <div class="col-md-12">
        <div class="main-title main-title2">
            <h2><span>What Our Patients Say</span> About Us</h2>
        </div>
        </div>
        </div>
        
        <div id="testimonials">
      <div class="container">
        <div class="row">
          
          <div class="col-md-12">
          <div class="span12">

            <div id="owl-demo2" class="owl-carousel">
              
              <div class="testi-sec">
                
                
        <img src="{{URL::to('frontend/images/testimonial-img1.jpg')}}" alt="">
                <div class="height10"></div>
        <span class="name">Elebana Front</span> 
                <span class="patient">Heart Patient</span>
                <div class="height30"></div>
                <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna  aliquam erat volutpat. vitae felis pretium, euismod ipsum nec, placerat turpis. Aenean eu gravida arcu, et consectetur  orci Quisque posuere dolor in malesuada fermentum.</p>                      <div class="height35"></div>
                
        
              </div>
              <div class="testi-sec">
                
        
        <img src="{{URL::to('frontend/images/testimonial-img2.jpg')}}" alt="">
                <div class="height10"></div>
        <span class="name">Johny Bravo</span> 
                <span class="patient">Hair Patient</span>
        <div class="height30"></div>
                <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna  aliquam erat volutpat. vitae felis pretium, euismod ipsum nec, placerat turpis. Aenean eu gravida arcu, et consectetur Quisque posuere dolor in malesuada fermentum sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna  aliquam erat volutpat.</p>             
                
                
                
              
              </div>
              <div class="testi-sec">
                <img src="{{URL::to('frontend/images/testimonial-img3.jpg')}}" alt="">
                <div class="height10"></div>
        <span class="name">Rubica Noi</span> 
                <span class="patient">Skin Patient</span>
        <div class="height30"></div>
        
                <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna  aliquam erat volutpat. vitae felis pretium, euismod ipsum nec, placerat turpis. Aenean eu gravida arcu, et consectetur  orci Quisque posuere dolor in malesuada fermentum.</p>                      <div class="height35"></div>
                
                
              
              </div>
        
        
            </div>

          </div>
          </div>
          
        </div>
      </div>
    </div>
    
    </div>
   </div>
   
   
   <!--End Testimonials-->
   
   </div>
   @endsection