@extends('frontend.master')

@section('content')

<!-- Mobile Menu Start -->
	<div class="container">
    <div id="page">
			<header class="header">
				<a href="#menu"></a>
				
			</header>
			
			<nav id="menu">
				<ul>
					<li><a href="#.">Home</a>
                    	<ul>
							<li> <a href="index.html">Home Page 1</a> </li>
							<li> <a href="index2.html">Home Page 2</a> </li>
							<li> <a href="index3.html">Home Page 3</a> </li>
                        </ul>
                    </li>
					<li><a href="#.">About us</a>
                    	<ul>
                        	<li> <a href="about-us.html">About Us</a> </li>
							<li> <a href="about-us2.html">About Us 2</a> </li>
                        </ul>
                    </li>
                    <li><a href="#.">Pages</a>
                    	<ul>
                        	<li> <a href="services.html">Services</a> </li>
							<li> <a href="services2.html">Services Two</a> </li>
							<li> <a href="appointment.html">Appointment</a> </li>
							<li> <a href="departments.html">Departments</a> </li>
							<li> <a href="patient-and-family.html">Patient and Family</a> </li>
							<li> <a href="team-members.html">Team Members One</a> </li>
							<li> <a href="team-members2.html">Team Members Two</a> </li>
							<li> <a href="research.html">Research</a> </li>
							<li> <a href="tables.html">Pricing tabels</a> </li>
                        </ul>
                    </li>
                    
                    <li><a href="procedures.html">Procedures</a></li>
					
					<li><a href="#.">Gallery</a>
                    	
						<ul>
                        	<li><a href="#.">Simple Gallery</a>
                            	<ul>
                                	<li> <a href="gallery-simple-two.html">Columns Two</a> </li>
                                    <li> <a href="gallery-simple-three.html">Columns Three</a> </li>
                                    <li> <a href="gallery-simple-four.html">Columns Four</a> </li>
                                </ul>
                            </li>
							
							<li><a href="#.">Nimble Gallery</a>
                            	<ul>
                                	<li> <a href="gallery-nimble-two.html">Columns Two</a> </li>
                                    <li> <a href="gallery-nimble-three.html">Columns Three</a> </li>
                                    <li> <a href="gallery-nimble-four.html">Columns Four</a> </li>
                                </ul>
                            </li>
                        </ul>
						
                    </li>
                    
                    
                    <li><a href="#.">News Posts</a>
                    	<ul>
                        	<li> <a href="news-sidebar.html">Sidebar</a> </li>
							<li> <a href="news-text.html">Text-Based</a> </li>
							<li> <a href="news-single.html">Single Post</a> </li>
							<li> <a href="news-double.html">Double Post</a> </li>
							<li> <a href="news-masonary.html">Masonary</a> </li>
                        </ul>
                    </li>
                    
					<li class="select"><a href="shop.html">Shop</a></li>
					
					<li><a href="#.">Contact Us</a>
                    	<ul>
                        	<li> <a href="contact-us.html">Contact-Us One</a> </li>
							<li> <a href="contact-us2.html">Contact-Us Two</a> </li>
                        </ul>
                    </li>
					
				</ul>
                
                
			</nav>
		</div>
		</div>
    <!-- Mobile Menu End -->

	
   <!--Start Banner-->
   
   <div class="sub-banner">
   
   	<img class="banner-img" src="{{URL::to('frontend/images/sub-banner.jpg')}}" alt="">
    <div class="detail">
    	<div class="container">
        	<div class="row">
            	<div class="col-md-12">
                	
                    <div class="paging">
                		<h2>Shop</h2>
						<ul>
						<li><a href="index.html">Home</a></li>
						<li><a>Shop</a></li>
						</ul>
                    </div>
                    
                </div>
            </div>
        </div>
    </div>
   
   </div>	
   
   <!--End Banner-->
   
    
   
   <!--Start Content-->
   <div class="content">
   
   
  
  <div class="services-content">
   		<div class="container">
        	
            
            <div class="row">
					<div class="col-md-9">
						<ul class="shop clearfix list-unstyled">
							<li>
								<div class="product">
									<div class="product-thumb">
										<a><img src="{{URL::to('frontend/images/shop/1.jpg')}}" alt=""></a>
									</div>
									<div class="product-description clearfix">
										<h3><a href="shop-detail.html">Adidas Winter Cap</a></h3>
										<p class="price">&pound;55.00</p>
										<span class="double-border"></span>
										<a href="#." class="product-cart-btn pull-left"><i class="icon-basket"></i> Add to cart</a>
										<a href="shop-detail.html" class="product-detail-btn pull-right"><i class="icon-browser"></i> Details</a>
									</div>
								</div>
							</li>
							<li>
								<div class="product">
									<div class="product-thumb">
										<label>Sale</label>
										<a><img src="{{URL::to('frontend/images/shop/2.jpg')}}" alt=""></a>
									</div>
									<div class="product-description clearfix">
										<h3><a href="shop-detail.html">Adidas Winter Cap</a></h3>
										<p class="price"><u>&pound;55.00</u> £24.00</p>
										<span class="double-border"></span>
										<a href="#." class="product-cart-btn pull-left"><i class="icon-basket"></i> Add to cart</a>
										<a href="shop-detail.html" class="product-detail-btn pull-right"><i class="icon-browser"></i> Details</a>
									</div>
								</div>
							</li>
							<li>
								<div class="product">
									<div class="product-thumb">
										<a><img src="{{URL::to('frontend/images/shop/3.jpg')}}" alt=""></a>
									</div>
									<div class="product-description clearfix">
										<h3><a href="shop-detail.html">Adidas Winter Cap</a></h3>
										<p class="price">&pound;55.00</p>
										<span class="double-border"></span>
										<a href="#." class="product-cart-btn pull-left"><i class="icon-basket"></i> Add to cart</a>
										<a href="shop-detail.html" class="product-detail-btn pull-right"><i class="icon-browser"></i> Details</a>
									</div>
								</div>
							</li>
							<li>
								<div class="product">
									<div class="product-thumb">
										<a><img src="{{URL::to('frontend/images/shop/4.jpg')}}" alt=""></a>
									</div>
									<div class="product-description clearfix">
										<h3><a href="shop-detail.html">Adidas Winter Cap</a></h3>
										<p class="price">&pound;55.00</p>
										<span class="double-border"></span>
										<a href="#." class="product-cart-btn pull-left"><i class="icon-basket"></i> Add to cart</a>
										<a href="shop-detail.html" class="product-detail-btn pull-right"><i class="icon-browser"></i> Details</a>
									</div>
								</div>
							</li>
							<li>
								<div class="product">
									<div class="product-thumb">
										<a><img src="{{URL::to('frontend/images/shop/5.jpg')}}" alt=""></a>
									</div>
									<div class="product-description clearfix">
										<h3><a href="shop-detail.html">Adidas Winter Cap</a></h3>
										<p class="price">&pound;55.00</p>
										<span class="double-border"></span>
										<a href="#." class="product-cart-btn pull-left"><i class="icon-basket"></i> Add to cart</a>
										<a href="shop-detail.html" class="product-detail-btn pull-right"><i class="icon-browser"></i> Details</a>
									</div>
								</div>
							</li>
							<li>
								<div class="product">
									<div class="product-thumb">
										<a><img src="{{URL::to('frontend/images/shop/6.jpg')}}" alt=""></a>
									</div>
									<div class="product-description clearfix">
										<h3><a href="shop-detail.html">Adidas Winter Cap</a></h3>
										<p class="price">&pound;55.00</p>
										<span class="double-border"></span>
										<a href="#." class="product-cart-btn pull-left"><i class="icon-basket"></i> Add to cart</a>
										<a href="shop-detail.html" class="product-detail-btn pull-right"><i class="icon-browser"></i> Details</a>
									</div>
								</div>
							</li>
							<li>
								<div class="product">
									<div class="product-thumb">
										<label>Sale</label>
										<a><img src="{{URL::to('frontend/images/shop/7.jpg')}}" alt=""></a>
									</div>
									<div class="product-description clearfix">
										<h3><a href="shop-detail.html">Adidas Winter Cap</a></h3>
										<p class="price"><u>&pound;55.00</u> £24.00</p>
										<span class="double-border"></span>
										<a href="#." class="product-cart-btn pull-left"><i class="icon-basket"></i> Add to cart</a>
										<a href="shop-detail.html" class="product-detail-btn pull-right"><i class="icon-browser"></i> Details</a>
									</div>
								</div>
							</li>
							<li>
								<div class="product">
									<div class="product-thumb">
										<a><img src="{{URL::to('frontend/images/shop/8.jpg')}}" alt=""></a>
									</div>
									<div class="product-description clearfix">
										<h3><a href="shop-detail.html">Adidas Winter Cap</a></h3>
										<p class="price">&pound;55.00</p>
										<span class="double-border"></span>
										<a href="#." class="product-cart-btn pull-left"><i class="icon-basket"></i> Add to cart</a>
										<a href="shop-detail.html" class="product-detail-btn pull-right"><i class="icon-browser"></i> Details</a>
									</div>
								</div>
							</li>
							<li>
								<div class="product">
									<div class="product-thumb">
										<a><img src="{{URL::to('frontend/images/shop/9.jpg')}}" alt=""></a>
									</div>
									<div class="product-description clearfix">
										<h3><a href="shop-detail.html">Adidas Winter Cap</a></h3>
										<p class="price">&pound;55.00</p>
										<span class="double-border"></span>
										<a href="#." class="product-cart-btn pull-left"><i class="icon-basket"></i> Add to cart</a>
										<a href="shop-detail.html" class="product-detail-btn pull-right"><i class="icon-browser"></i> Details</a>
									</div>
								</div>
							</li>
						</ul>
					</div>
					<div class="col-md-3">
						<aside>
							<div class="sidebar-widget no-margin">
								<h3>FILTER BY PRICE</h3>
								<div id="slider-range" class="ui-slider"></div>
								<div class="price-range clearfix">
									<p>Filter</p>
									<input type="text" readonly="" id="amount">
								</div>
							</div>
							<hr class="margin-top-40 margin-bottom-40">
							<div class="sidebar-widget">
								<h3>PRODUCT CATEGORIES</h3>
								<ul class="list-arrow list-unstyled">
									<li><a href="#."><i class="fa fa-angle-right"></i>T-shirt</a></li>
									<li><a href="#."><i class="fa fa-angle-right"></i>Shose</a></li>
									<li><a href="#."><i class="fa fa-angle-right"></i>Winter Cap</a></li>
									<li><a href="#."><i class="fa fa-angle-right"></i>Men Suite</a></li>
									<li><a href="#."><i class="fa fa-angle-right"></i>Women Rings</a></li>
								</ul>
							</div>
							<hr class="margin-top-40 margin-bottom-40">
							<div class="sidebar-widget">
								<h3>TOP PRODUCTS</h3>
								<div class="top-products clearfix">
									<a href="#."><img src="{{URL::to('frontend/images/shop/top-products-1.jpg')}}" alt=""></a>
									<div class="top-products-detail">
										<h4><a href="#.">Woo Ninja</a></h4>
										<p>£55.00</p>
										<div class="ratings">
											<i class="fa fa-star"></i>
											<i class="fa fa-star"></i>
											<i class="fa fa-star"></i>
											<i class="fa fa-star"></i>
											<i class="fa fa-star"></i>
										</div>
									</div>
								</div>
								<div class="top-products clearfix">
									<a href="#."><img src="{{URL::to('frontend/images/shop/top-products-2.jpg')}}" alt=""></a>
									<div class="top-products-detail">
										<h4><a href="#.">Woo Ninja</a></h4>
										<p>£55.00</p>
										<div class="ratings">
											<i class="fa fa-star"></i>
											<i class="fa fa-star"></i>
											<i class="fa fa-star"></i>
											<i class="fa fa-star"></i>
											<i class="fa fa-star"></i>
										</div>
									</div>
								</div>
								<div class="top-products clearfix">
									<a href="#."><img src="{{URL::to('frontend/images/shop/top-products-3.jpg')}}" alt=""></a>
									<div class="top-products-detail">
										<h4><a href="#.">Woo Ninja</a></h4>
										<p>£55.00</p>
										<div class="ratings">
											<i class="fa fa-star"></i>
											<i class="fa fa-star"></i>
											<i class="fa fa-star"></i>
											<i class="fa fa-star"></i>
											<i class="fa fa-star"></i>
										</div>
									</div>
								</div>
							</div>
						</aside>
					</div>
				</div>
            
            
        </div>
   </div>




@stop