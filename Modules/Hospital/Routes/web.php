<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::prefix('hospital')->group(function() {
   // Route::get('/details', 'user_dashboard\HospitalController@index')->name('hospital.detail');

    Route::get('/appointment', 'user_dashboard\HospitalController@appointment')->name('hospital.appointment');
      
    Route::get('/appointment_history', 'user_dashboard\HospitalController@appointment_history')->name('hospital.appointment_history');
     Route::get('/upload', 'user_dashboard\HospitalController@upload_doc')->name('hospital.upload');
});




//for Book Appointment
Route::get('hospitals/book_appointment', 'user_dashboard\HospitalController@book_appointment')->name('hospital.book_appointment');

// Website...............................................................................

Route::get('/hospitals', 'website\HospitalController@index')->name('web.hospitals');
Route::get('/hospitals/detail', 'website\HospitalController@hospital_detail')->name('web.hospital_detail');


