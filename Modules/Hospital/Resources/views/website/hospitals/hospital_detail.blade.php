@extends('frontend.master')

@section('content')

<!-- Mobile Menu Start -->
	@include('frontend.layouts.mobile_menu')
    <!-- Mobile Menu End -->

	
   <!--Start Banner-->
   
   <div class="sub-banner">
   
   	<img class="banner-img" src="{{URL::to('frontend/images/sub-banner.jpg')}}" alt="">
    <div class="detail">
    	<div class="container">
        	<div class="row">
            	<div class="col-md-12">
                	
                    <div class="paging">
                		<h2>Hospital Details 
                		<a href="{{route('hospital.book_appointment')}}"  class="btn btn-primary btn pull-right" role="button" style="font-size: 20px">Book Appointment</a></h2>
						<ul>
						<li><a href="{{route('home')}}">Home</a></li>
						<li><a href="{{route('web.hospitals')}}">Hospitals</a></li>
						<li><a>Hospital Details</a></li>
						</ul>
                    </div>
                    
                </div>
            </div>
        </div>
    </div>
   
   </div>	
   
   <!--End Banner-->
   
    
   
   <!--Start Content-->
   <div class="content">
   
   
  
  <div class="services-content">
   		<div class="container">
        	
            
            <div class="row">
					<div class="col-md-5">
						<div class="product-detail-slider">
							<div class="rslides" id="product-detail-slider">
								<img src="{{URL::to('frontend/images/shop/product-detail-img1.jpg')}}" alt="">
								<img src="{{URL::to('frontend/images/shop/product-detail-img1.jpg')}}" alt="">
								<img src="{{URL::to('frontend/images/shop/product-detail-img1.jpg')}}" alt="">
							</div>
						</div>
					</div>
					<div class="col-md-7">
						<div class="product-detail-description">
							<h2>Hospital Name</h2>
							
							<p class="price big dark">Description</p>
							<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc pharetra euismod enim, ac ultricies nunc fringilla nec. Nulla cursus commodo risus, quis consectetur risus commodo fringilla. Fusce sapien urna, ornare sit sodaltes sed nunc. </p>
							
							
						<div class="time-table-sec">
					    <ul id="accordion2" class="accordion2">
					      <li>
					      <ul class="submenu time-table">
					      <li class="tit"><h5>Working Time</h5></li>
					      <li><span class="day">Monday - Friday</span> <span class="divider">-</span> <span class="time">8.00 - 16.00</span></li>
					      <li><span class="day">Saturday</span> <span class="divider">-</span> <span class="time">9.15 - 17.00</span></li>
					      <li><span class="day">Sunday</span> <span class="divider">-</span> <span class="time">9.45 - 15.00</span></li>
					      </ul>
					      <div class="link" ><img  class="time-tab" src="{{URL::to('frontend/images/timetable-menu.png')}}" alt=""></div>

					      </li>
					    </ul>
					  </div>

						</div>
					</div>
				</div>
				

 
				<div class="clearfix verticalTab" id="target">
					<ul class="resp-tabs-list">
						<li>Branches</li>
						<li>Additional Information</li>
						<li>3 Review</li>
					</ul>
					<div class="resp-tabs-container">
						<div class="investigation">
							<div class="investigation-sec">
						
								<div class="title">
									<span class="left">Address</span>
									<span class="right">Contact No</span>
								</div>
								
								<div class="detail light-bg">
									<span class="left">Tummy Tuck</span>
									<span class="right">$155</span>
								</div>
								
								<div class="detail dark-bg">
									<span class="left">Liposuction</span>
									<span class="right">$120</span>
								</div>
								
								<div class="detail light-bg">
									<span class="left">Cardiac ablation</span>
									<span class="right">$100 - $255</span>
								</div>
								
								<div class="detail dark-bg">
									<span class="left">Trunk Liposuction</span>
									<span class="right">$318</span>
								</div>
								
								<div class="detail light-bg">
									<span class="left">Colonoscopy</span>
									<span class="right">$786</span>
								</div>
								
								<div class="detail dark-bg">
									<span class="left">Heart Patient</span>
									<span class="right">$1200</span>
								</div>
								
							</div>
						</div>
						<div class="investigation">
							<div class="investigation-sec">
						
								<div class="title">
									<span class="left">Test Name</span>
									<span class="right">Price</span>
								</div>
								
								<div class="detail light-bg">
									<span class="left">Tummy Tuck</span>
									<span class="right">$155</span>
								</div>
								
								<div class="detail dark-bg">
									<span class="left">Liposuction</span>
									<span class="right">$120</span>
								</div>
								
								<div class="detail light-bg">
									<span class="left">Cardiac ablation</span>
									<span class="right">$100 - $255</span>
								</div>
								
								<div class="detail dark-bg">
									<span class="left">Trunk Liposuction</span>
									<span class="right">$318</span>
								</div>
								
								<div class="detail light-bg">
									<span class="left">Colonoscopy</span>
									<span class="right">$786</span>
								</div>
								
								<div class="detail dark-bg">
									<span class="left">Heart Patient</span>
									<span class="right">$1200</span>
								</div>
								
							</div>

						</div>
						<div>
							<div class="review" id="comments">
								<ol class="commentlist">
									<li class="comment">
										<div class="comment_container">
											<div class="comment-avartar">
												<img width="60" height="60" alt="" src="{{URL::to('frontend/images/comment-img.jpg')}}">
											</div>
											<div class="comment-text">
												<div class="ratings" title="Rated 5 out of 5">
													<ul class="list-unstyled">
														<li><a href="#."><i class="icon-star-full"></i></a></li>
														<li><a href="#."><i class="icon-star-full"></i></a></li>
														<li><a href="#."><i class="icon-star-full"></i></a></li>
														<li><a href="#."><i class="icon-star-full"></i></a></li>
														<li><a href="#."><i class="icon-star-full"></i></a></li>
													</ul>
												</div>
												<p class="meta">
													<strong>Admin</strong> &ndash; December 12, 2013:
												</p>
												<div class="description"><p>Nice</p></div>
											</div>
										</div>
									</li>
								</ol>
								<br>
								<a class="btn btn-default btn-dark btn-medium btn-square scroll" id="add-review-btn" href="#add-review-btn">Add a review</a>
								<div class="add-review-form" id="add-review-form" style="display:none">
									<a href="#target" class="review-form-close scroll" id="review-form-close"><i class="icon-cross2"></i></a>
									<div class="review_form_thumb text-center">
										<img src="{{URL::to('frontend/images/add-review-img.jpg')}}" class="img-circle" alt="">
									</div>
									<div class="comment-respond no-padding">
										<h2 class="text-center">Add a review</h2>
										<form action="#" name="add_review_form" id="add_review_form">
											<input type="text" placeholder="Name *" class="validate[required]" data-prompt-position="topLeft:0">
											<input type="text" placeholder="Email *" class="validate[required,custom[email]]" data-prompt-position="topLeft:0">
											<div class="your-rating">
												<label>Your Rating</label>
												<ul class="rating clearfix">
													<li class="icon-star-full"></li>
													<li class="icon-star-full"></li>
													<li class="icon-star-full"></li>
													<li class="icon-star-full"></li>
													<li class="icon-star-full"></li>
												</ul>
											</div>
											<textarea placeholder="Your Review" class="validate[required]" data-prompt-position="topLeft:0"></textarea>
											<input type="submit" class="btn btn-medium btn-dark btn-square" value="submit">
										</form>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				
				
				
            
            
        </div>
   </div>
  
   
   </div>
   <!--End Content-->

@stop