<div class="vobilet-navbar" id="headerMenuCollapse">
					<div class="container">
						<ul class="nav">
							<li class="nav-item ">
								<a class="nav-link @if(request()->is('hospital/appointment')) active @endif" href="{{route('hospital.appointment')}}">
									<i class="fa fa-user-md"></i>
									<span>APPOINTMENT</span>
								</a>
								
							</li>
							
							<li class="nav-item">
								<a class="nav-link @if(request()->is('hospital/appointment_history')) active @endif" href="{{route('hospital.appointment_history')}}">
									<i class="fa fa-history"></i>
									<span>APPOINTMENT HISTORY</span>
								</a>
							</li>
							<li class="nav-item ">
								<a class="nav-link @if(request()->is('hospital/upload')) active @endif" href="{{route('hospital.upload')}}"><i class="fa fa-upload"></i> <span>UPLOAD DOCUMENT</span></a>
								
								<!-- dropdown-menu -->
							</li>
							
						</ul>
					</div>
				</div>