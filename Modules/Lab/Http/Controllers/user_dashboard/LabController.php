<?php

namespace Modules\Lab\Http\Controllers\user_dashboard;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;

class LabController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    //--------Start functions of Manage labs------
    public function appointment()
    {
        return view('lab::user_dashboard.lab_detail.appointment');
    }
   
    public function appointment_history()
    {
        return view('lab::user_dashboard.lab_detail.appointment_history');
    }
     public function upload_doc()
    {
        return view('lab::user_dashboard.lab_detail.upload_doc');
    }
//--------End function of Manage labs------
    public function book_appointment()
    {
        return view('lab::user_dashboard.book_appointment');
    }
    public function create()
    {
        return view('lab::create');
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Show the specified resource.
     * @param int $id
     * @return Response
     */
    public function show($id)
    {
        return view('lab::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Response
     */
    public function edit($id)
    {
        return view('lab::edit');
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Response
     */
    public function destroy($id)
    {
        //
    }
}
