<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::prefix('lab')->group(function() {
  //  Route::get('/details', 'user_dashboard\LabController@index')->name('lab.detail');
     Route::get('/appointment', 'user_dashboard\LabController@appointment')->name('lab.appointment');
     
    Route::get('/appointment_history', 'user_dashboard\LabController@appointment_history')->name('lab.appointment_history');
     Route::get('/upload', 'user_dashboard\LabController@upload_doc')->name('lab.upload');
   
});
//for Book Appointment
Route::get('labs/book_appointment', 'user_dashboard\LabController@book_appointment')->name('lab.book_appointment');

// Website.............................................................................
Route::get('/labs', 'website\LabController@index')->name('web.labs');
Route::get('labs/detail', 'website\LabController@lab_detail')->name('web.lab_detail');
