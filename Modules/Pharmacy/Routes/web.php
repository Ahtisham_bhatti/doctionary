<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// User Dashboard............................................................................

Route::prefix('pharmacy')->group(function() {

//Route::get('/details', 'user_dashboard\PharmacyController@index')->name('pharmacy.detail');

Route::get('/order', 'user_dashboard\PharmacyController@order')->name('pharmacy.order');
Route::get('/order_history', 'user_dashboard\PharmacyController@order_history')->name('Pharmacy.order_history');
Route::get('/upload', 'user_dashboard\PharmacyController@upload_doc')->name('Pharmacy.upload');
});


//for order medicine
Route::get('pharmacies/order_medicine', 'user_dashboard\PharmacyController@order_medicine')->name('order.medicine');



// Website.....................................................................................

Route::get('/pharmacies', 'website\PharmacyController@index')->name('web.pharmacies');
Route::get('/pharmacies/detail', 'website\PharmacyController@pharmacy_detail')->name('web.pharmacy_detail');