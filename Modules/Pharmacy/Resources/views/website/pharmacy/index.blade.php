@extends('frontend.master')

@section('content')




	
	
	<!-- Mobile Menu Start -->
	<div class="container">
    <div id="page">
			<header class="header">
				<a href="#menu"></a>
				
			</header>
			
			<nav id="menu">
				<ul>
					<li><a href="#.">Home</a>
                    	<ul>
							<li> <a href="index.html">Home Page 1</a> </li>
							<li> <a href="index2.html">Home Page 2</a> </li>
							<li> <a href="index3.html">Home Page 3</a> </li>
							<li> <a href="index4.html">Home Page 4</a> </li>
                        </ul>
                    </li>
					<li><a href="#.">About us</a>
                    	<ul>
                        	<li> <a href="about-us.html">About Us</a> </li>
							<li> <a href="about-us2.html">About Us 2</a> </li>
                        </ul>
                    </li>
                    <li><a href="#.">Pages</a>
                    	<ul>
                        	<li> <a href="services.html">Services</a> </li>
							<li> <a href="services2.html">Services Two</a> </li>
							<li> <a href="appointment.html">Appointment</a> </li>
							<li> <a href="departments.html">Departments</a> </li>
							<li> <a href="patient-and-family.html">Patient and Family</a> </li>
							<li> <a href="team-members.html">Team Members One</a> </li>
							<li> <a href="team-members2.html">Team Members Two</a> </li>
							<li> <a href="research.html">Research</a> </li>
							<li> <a href="tables.html">Pricing tabels</a> </li>
                        </ul>
                    </li>
                    
                    <li><a href="procedures.html">Procedures</a></li>
					
					<li class="select"><a href="#.">Gallery</a>
                    	
						<ul>
                        	<li class="select"><a href="#.">Simple Gallery</a>
                            	<ul>
                                	<li> <a href="gallery-simple-two.html">Columns Two</a> </li>
                                    <li> <a href="gallery-simple-three.html">Columns Three</a> </li>
                                    <li class="select"> <a href="gallery-simple-four.html">Columns Four</a> </li>
                                </ul>
                            </li>
							
							<li><a href="#.">Nimble Gallery</a>
                            	<ul>
                                	<li> <a href="gallery-nimble-two.html">Columns Two</a> </li>
                                    <li> <a href="gallery-nimble-three.html">Columns Three</a> </li>
                                    <li> <a href="gallery-nimble-four.html">Columns Four</a> </li>
                                </ul>
                            </li>
                        </ul>
						
                    </li>
                    
                    
                    <li><a href="#.">News Posts</a>
                    	<ul>
                        	<li> <a href="news-sidebar.html">Sidebar</a> </li>
							<li> <a href="news-text.html">Text-Based</a> </li>
							<li> <a href="news-single.html">Single Post</a> </li>
							<li> <a href="news-double.html">Double Post</a> </li>
							<li> <a href="news-masonary.html">Masonary</a> </li>
                        </ul>
                    </li>
                    
					<li><a href="shop.html">Shop</a></li>
					
					<li><a href="#.">Contact Us</a>
                    	<ul>
                        	<li> <a href="contact-us.html">Contact-Us One</a> </li>
							<li> <a href="contact-us2.html">Contact-Us Two</a> </li>
                        </ul>
                    </li>
					
				</ul>
                
                
			</nav>
		</div>
		</div>
    <!-- Mobile Menu End -->

   <!--Start Banner-->
   
   <div class="sub-banner">
   
   	<img class="banner-img" src="{{URL::to('frontend/images/sub-banner.jpg')}}" alt="">
    <div class="detail">
    	<div class="container">
        	<div class="row">
            	<div class="col-md-12">
                	
                    <div class="paging">
                		<h2>PHARMACY
                			<a href="{{route('order.medicine')}}"  class="btn btn-primary btn pull-right" role="button" style="font-size: 20px">Order Medicine</a>
						</h2>

						<ul>
						<li><a href="{{route('home')}}">Home</a></li>
						<li><a>Pharmacy</a></li>
						</ul>
                    </div>
                    
                </div>
            </div>
        </div>
    </div>
   
   </div>	
   
   <!--End Banner-->
   <!--Start Search bar-->
   <div class="container">
  
        		<div class="news-posts">
        		<div class="search" style="margin: 0px;">
					<input type="text" name=" " value="Search here">
					<a href="#."><i class="icon-search"></i></a>
				</div>
			</div>
        
    </div>
    <!--end  Search bar-->
   
   <!--Start Content-->
   <div class="content">
   
   
  
  <div class="gallery">
   		<div class="container">

            <div class="row">
	        <div class="col-md-12">
            <div class="main-title">
                <h2><span>All</span> Pharmacies</h2>
                <p>If you need a doctor for to consectetuer Lorem ipsum dolor, consectetur adipiscing elit. Ut volutpat eros  adipiscing nonummy.</p>
            </div>
            </div>
            </div>
            
			<div class="row">
				<div class="main-gallery">
					
					<div class="col-md-3">
						<a class="gallery-sec fancybox photo-icon" href="{{route('order.medicine')}}" data-fancybox-group="gallery">		
                        	<div class="image-hover img-layer-slide-left-right">
							<img src="{{URL::to('frontend/images/Pharmacy/pharmacy1.jpg')}}" alt="">
                            <div class="layer"> <i style="width: 48%;margin-left: -63px;height: 51px;font-size: 14px;font-family: 'Source Sans Pro', sans-serif; font-weight: 600;">Order Medicine</i> </div>
                            </div>
                            </a>

                            <a class="gallery-sec fancybox photo-icon" href="{{route('web.pharmacy_detail')}}" data-fancybox-group="gallery">	
                            
							<div class="detail" style="margin-top: -50px;">
								<h6>SERVAI+</h6>
								<span>Cras porttitor mauris pulvinar</span>
							</div>
						</a>
						
					</div>
                    
					
					<div class="col-md-3">
						<a class="gallery-sec" href="{{route('order.medicine')}}" data-fancybox-group="gallery">		
                        	<div class="image-hover img-layer-slide-left-right">
							<img src="{{URL::to('frontend/images/Pharmacy/pharmacy2.jpg')}}" alt="">
                            <div class="layer"> <i style="width: 48%;margin-left: -63px;height: 51px;font-size: 14px;font-family: 'Source Sans Pro', sans-serif;font-weight: 600;">Order Medicine</i> </div>
                            </div>
                            </a>

                            <a class="gallery-sec fancybox photo-icon" href="{{route('web.pharmacy_detail')}}" data-fancybox-group="gallery">	
                            
							<div class="detail" style="margin-top: -50px;">
								<h6>FAZAL DIN'S</h6>
								<span>Cras porttitor mauris pulvinar</span>
							</div>
						</a>
						
					</div>
					
					
					<div class="col-md-3">
						<a class="gallery-sec fancybox photo-icon" href="{{route('order.medicine')}}" data-fancybox-group="gallery">		
                        	<div class="image-hover img-layer-slide-left-right">
							<img src="{{URL::to('frontend/images/Pharmacy/pharmacy3.jpg')}}" alt="">
                            <div class="layer"> <i style="width: 48%;margin-left: -63px;height: 51px;font-size: 14px;font-family: 'Source Sans Pro', sans-serif;font-weight: 600;">Order Medicine</i> </div>
                            </div>
                            </a>

                            <a class="gallery-sec fancybox photo-icon" href="{{route('web.pharmacy_detail')}}" data-fancybox-group="gallery">	
                            
							<div class="detail" style="margin-top: -50px;">
								<h6>Med-X PHARMACY</h6>
								<span>Cras porttitor mauris pulvinar</span>
							</div>
						</a>
						
					</div>
					
					
					
					<div class="col-md-3">
						<a class="gallery-sec fancybox photo-icon" href="{{route('order.medicine')}}" data-fancybox-group="gallery">		
                        	<div class="image-hover img-layer-slide-left-right">
							<img src="{{URL::to('frontend/images/Pharmacy/pharmacy4.jpg')}}" alt="">
                            <div class="layer"> <i style="width: 48%;margin-left: -63px;height: 51px;font-size: 14px;font-family: 'Source Sans Pro', sans-serif;font-weight: 600;">Order Medicine</i> </div>
                            </div>
                            </a>

                            <a class="gallery-sec fancybox photo-icon" href="{{route('web.pharmacy_detail')}}" data-fancybox-group="gallery">	
                            
							<div class="detail" style="margin-top: -50px;">
								<h6>LIFE CARE PHARMACY</h6>
								<span>Cras porttitor mauris pulvinar</span>
							</div>
						</a>
						
					</div>
					
					
					
					<div class="col-md-3">
						<a class="gallery-sec fancybox photo-icon" href="{{route('order.medicine')}}" data-fancybox-group="gallery">		
                        	<div class="image-hover img-layer-slide-left-right">
							<img src="{{URL::to('frontend/images/Pharmacy/pharmacy5.jpg')}}" alt="">
                            <div class="layer"> <i style="width: 48%;margin-left: -63px;height: 51px;font-size: 14px;font-family: 'Source Sans Pro', sans-serif;font-weight: 600;">Order Medicine</i> </div>
                            </div>
                            </a>

                            <a class="gallery-sec fancybox photo-icon" href="{{route('web.pharmacy_detail')}}" data-fancybox-group="gallery">	
                            
							<div class="detail" style="margin-top: -50px;">
								<h6>CARE PHARMACY</h6>
								<span>Cras porttitor mauris pulvinar</span>
							</div>
						</a>
						
					</div>
					
					
					
					<div class="col-md-3">
						<a class="gallery-sec fancybox photo-icon" href="{{route('order.medicine')}}" data-fancybox-group="gallery">		
                        	<div class="image-hover img-layer-slide-left-right">
							<img src="{{URL::to('frontend/images/Pharmacy/pharmacy6.jpg')}}" alt="">
                            <div class="layer"> <i style="width: 48%;margin-left: -63px;height: 51px;font-size: 14px;font-family: 'Source Sans Pro', sans-serif;font-weight: 600;">Order Medicine</i> </div>
                            </div>
                            </a>

                            <a class="gallery-sec fancybox photo-icon" href="{{route('web.pharmacy_detail')}}" data-fancybox-group="gallery">	
                            
							<div class="detail" style="margin-top: -50px;">
								<h6>KHALID PHARMACY</h6>
								<span>Cras porttitor mauris pulvinar</span>
							</div>
						</a>
						
					</div>
					
					
					
					<div class="col-md-3">
						<a class="gallery-sec fancybox photo-icon" href="{{route('order.medicine')}}" data-fancybox-group="gallery">		
                        	<div class="image-hover img-layer-slide-left-right">
							<img src="{{URL::to('frontend/images/Pharmacy/pharmacy7.jpg')}}" alt="">
                            <div class="layer"> <i style="width: 48%;margin-left: -63px;height: 51px;font-size: 14px;font-family: 'Source Sans Pro', sans-serif;font-weight: 600;">Order Medicine</i> </div>
                            </div>
                            </a>

                            <a class="gallery-sec fancybox photo-icon" href="{{route('web.pharmacy_detail')}}" data-fancybox-group="gallery">	
                            
							<div class="detail" style="margin-top: -50px;">
								<h6>HEALTH MART</h6>
								<span>Cras porttitor mauris pulvinar</span>
							</div>
						</a>
						
					</div>
					
					
					
					<div class="col-md-3">
						<a class="gallery-sec fancybox photo-icon" href="{{route('order.medicine')}}" data-fancybox-group="gallery">		
                        	<div class="image-hover img-layer-slide-left-right">
							<img src="{{URL::to('frontend/images//Pharmacy/pharmacy8.jpg')}}" alt="">
                            <div class="layer"> <i style="width: 48%;margin-left: -63px;height: 51px;font-size: 14px;font-family: 'Source Sans Pro', sans-serif;font-weight: 600;">Order Medicine</i> </div>
                            </div>
                            </a>

                            <a class="gallery-sec fancybox photo-icon" href="{{route('web.pharmacy_detail')}}" data-fancybox-group="gallery">	
                            
							<div class="detail" style="margin-top: -50px;">
								<h6>PAKISTAN PHARMACIST</h6>
								<span>Cras porttitor mauris pulvinar</span>
							</div>
						</a>
						
					</div>

					<div class="col-md-3">
						<a class="gallery-sec fancybox photo-icon" href="{{route('order.medicine')}}" data-fancybox-group="gallery">		
                        	<div class="image-hover img-layer-slide-left-right">
							<img src="{{URL::to('frontend/images/Pharmacy/pharmacy9.jpg')}}" alt="">
                            <div class="layer"> <i style="width: 48%;margin-left: -63px;height: 51px;font-size: 14px;font-family: 'Source Sans Pro', sans-serif;font-weight: 600;">Order Medicine</i> </div>
                            </div>
                            </a>

                            <a class="gallery-sec fancybox photo-icon" href="{{route('web.pharmacy_detail')}}" data-fancybox-group="gallery">	
                            
							<div class="detail" style="margin-top: -50px;">
								<h6>CLINIX</h6>
								<span>Cras porttitor mauris pulvinar</span>
							</div>
						</a>
						
					</div>

					<div class="col-md-3">
						<a class="gallery-sec fancybox photo-icon" href="{{route('order.medicine')}}" data-fancybox-group="gallery">		
                        	<div class="image-hover img-layer-slide-left-right">
							<img src="{{URL::to('frontend/images/Pharmacy/pharmacy10.jpg')}}" alt="">
                            <div class="layer"> <i style="width: 48%;margin-left: -63px;height: 51px;font-size: 14px;font-family: 'Source Sans Pro', sans-serif;font-weight: 600;">Order Medicine</i> </div>
                            </div>
                            </a>

                            <a class="gallery-sec fancybox photo-icon" href="{{route('web.pharmacy_detail')}}" data-fancybox-group="gallery">	
                            
							<div class="detail" style="margin-top: -50px;">
								<h6>GREEN PHARMACY</h6>
								<span>Cras porttitor mauris pulvinar</span>
							</div>
						</a>
						
					</div>

					<div class="col-md-3">
						<a class="gallery-sec fancybox photo-icon" href="{{route('order.medicine')}}" data-fancybox-group="gallery">		
                        	<div class="image-hover img-layer-slide-left-right">
							<img src="{{URL::to('frontend/images/Pharmacy/pharmacy11.jpg')}}" alt="">
                            <div class="layer"> <i style="width: 48%;margin-left: -63px;height: 51px;font-size: 14px;font-family: 'Source Sans Pro', sans-serif;font-weight: 600;">Order Medicine</i> </div>
                            </div>
                            </a>

                            <a class="gallery-sec fancybox photo-icon" href="{{route('web.pharmacy_detail')}}" data-fancybox-group="gallery">	
                            
							<div class="detail" style="margin-top: -50px;">
								<h6>B PHARMACY</h6>
								<span>Cras porttitor mauris pulvinar</span>
							</div>
						</a>
						
					</div>
					<div class="col-md-3">
						<a class="gallery-sec fancybox photo-icon" href="{{route('order.medicine')}}" data-fancybox-group="gallery">		
                        	<div class="image-hover img-layer-slide-left-right">
							<img src="{{URL::to('frontend/images/Pharmacy/pharmacy12.jpg')}}" alt="">
                            <div class="layer"> <i style="width: 48%;margin-left: -63px;height: 51px;font-size: 14px;font-family: 'Source Sans Pro', sans-serif;font-weight: 600;">Order Medicine</i> </div>
                            </div>
                            </a>

                            <a class="gallery-sec fancybox photo-icon" href="{{route('web.pharmacy_detail')}}" data-fancybox-group="gallery">	
                            
							<div class="detail" style="margin-top: -50px;">
								<h6>SAVER PHARMACY</h6>
								<span>Cras porttitor mauris pulvinar</span>
							</div>
						</a>
						
					</div>
					
					
					
                    
					
					
					
					<div class="col-md-12">
						<div class="paging">
							<a href="#." class="selected">1</a>
							<a href="#">2</a>
							<a href="#">3</a>
						</div>
					</div>
					
				</div>
			</div>
            
            
        </div>
   </div>
  
   
 
   <!--End Content-->
   
   
   
   





  </div>

<div class="modal fade" id="modalContactForm" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
  aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header text-center">
        <h4 class="modal-title w-100 font-weight-bold">Write to us</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body mx-3">
        <div class="md-form mb-5">
          <i class="fas fa-user prefix grey-text"></i>
          <input type="text" id="form34" class="form-control validate">
          <label data-error="wrong" data-success="right" for="form34">Your name</label>
        </div>

        <div class="md-form mb-5">
          <i class="fas fa-envelope prefix grey-text"></i>
          <input type="email" id="form29" class="form-control validate">
          <label data-error="wrong" data-success="right" for="form29">Your email</label>
        </div>

        <div class="md-form mb-5">
          <i class="fas fa-tag prefix grey-text"></i>
          <input type="text" id="form32" class="form-control validate">
          <label data-error="wrong" data-success="right" for="form32">Subject</label>
        </div>

        <div class="md-form">
          <i class="fas fa-pencil prefix grey-text"></i>
          <textarea type="text" id="form8" class="md-textarea form-control" rows="4"></textarea>
          <label data-error="wrong" data-success="right" for="form8">Your message</label>
        </div>

      </div>
      <div class="modal-footer d-flex justify-content-center">
        <button class="btn btn-unique">Send <i class="fas fa-paper-plane-o ml-1"></i></button>
      </div>
    </div>
  </div>
</div>


@stop