@extends('frontend.master')

@section('content')
	<div class="container">
		<h2>MEDICINE ORDER</h2>

		<form>
		<div class="row" style="margin-top: 15px;background-color: white;" >

			<div class="col-md-6">
			  <div class="form-group">
			    <label><i class="fa fa-user margin-right-10px"></i> Patient Name</label>
			    <input type="text" name="full_name" required id="Patient_Name" class="form-control" placeholder="Enter patient name">
			  </div>
			</div>
			<div class="col-md-6">
			  <div class="form-group">
			      <label><i class="fa fa-phone margin-right-10px"></i> Mobile No </label>
			      <input type="number" name="mobile_no" required id="Mobile_No" class="form-control international-inputmask" value="92" min-length="12" max-length="12" placeholder="Enter phone no#.">
			      <span class="opacity-5">Example : 923123456789</span>
			    </div>
			 </div>
		 
		</div>
		<div class="row" style="margin-top: 30px;background-color: white;">

		<div class="col-md-6">
			<div class="row">
			  <div class="col-md-12">
			    <div class="form-group" >
			      <label for="imageFile">Upload Prescription</label>
			        <div class="d-flex justify-content-center">
			          <div class="btn btn-mdb-color btn-rounded float-left">
			            <span>Choose file</span>
			            <div class="col-md-12" >
			              <input type="file" name="file" id="imageFile" accept="image/*">
			            </div>
			           
			          </div>
			        </div>
			      </div>
			    </div>
			  </div>
		    
		      <div class="row"> 
			        <div class="col-md-8">                   
			          <div class="form-group">
			          <label for="dob">Medicine Name</label>
			          <select class="form-control select2" name="medicine" id="medicineInput">
			            <option value="default"> Search Medicine</option> 
			          </select>
			          </div>
			      </div>
		        <div class="col-md-4">
		            <label for="quantity">Quantity</label>
		            <input type="number" name="quantity" class="form-control" id="MedicineQuantity" min="1" value="1">
		          </div>
		      
		        </div>
		        <div class="row">
		          <div class="col-md-12 text-center">
		              <button id="addMedicine" class="btn btn-primary waves-effect waves-light m-r-10">Add</button>
		            </div>
		            <br>
		           
		        </div>
		         <div class="row">
		          
		            <br>
		            <div class="col-md-12">
		              <button type="submit" id="presSubmit" class="btn waves-effect waves-light btn-block btn-primary"> Proceed Checkout </button>
		              <br>
		              <div class="col-md-12" id="medCard">
		              </div>
		            </div>
		        </div>

		    
		   
		</div>
			<div class="col-md-6">
			    
			  </div>
		 
		</div>


		</form>
	</div>
@endsection