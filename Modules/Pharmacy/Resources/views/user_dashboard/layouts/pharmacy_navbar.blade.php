<div class="vobilet-navbar" id="headerMenuCollapse">
					<div class="container">
						<ul class="nav">
							<li class="nav-item ">
								<a class="nav-link @if(request()->is('pharmacy/order')) active @endif" href="{{route('pharmacy.order')}}">
									<i class="fa fa-first-order"></i>
									<span>ORDERS</span>
								</a>
								
							</li>
							<li class="nav-item">
								<a class="nav-link @if(request()->is('pharmacy/order_history')) active @endif" href="{{route('Pharmacy.order_history')}}">
									<i class="fa fa-history"></i>
									<span>ORDERS HISTORY</span>
								</a>
							</li>
							<li class="nav-item @if(request()->is('pharmacy/upload')) active @endif">
								<a class="nav-link" href="{{route('Pharmacy.upload')}}"><i class="fa fa-upload"></i> <span>UPLOAD DOCUMENT</span></a>
								
								<!-- dropdown-menu -->
							</li>
							
						</ul>
					</div>
				</div>