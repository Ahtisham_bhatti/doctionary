<?php

namespace Modules\Pharmacy\Http\Controllers\user_dashboard;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;


class PharmacyController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        //return view('pharmacy::user_dashboard.pharmacy_detail');
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    //--------Start functions of Manage pharmacies------
   
    public function order()
    {
        return view('pharmacy::user_dashboard.pharmacy_detail.order');
    }

    public function order_history()
    {
        return view('pharmacy::user_dashboard.pharmacy_detail.order_history');
    }
     public function upload_doc()
    {
        return view('pharmacy::user_dashboard.pharmacy_detail.upload_doc');
    }
//--------End function of Manage pharmacies------
    public function order_medicine()
    {
        return view('pharmacy::user_dashboard.order_medicine');
    }
    public function create()
    {
        return view('pharmacy::create');
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Show the specified resource.
     * @param int $id
     * @return Response
     */
    public function show($id)
    {
        return view('pharmacy::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Response
     */
    public function edit($id)
    {
        return view('pharmacy::edit');
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Response
     */
    public function destroy($id)
    {
        //
    }
}
