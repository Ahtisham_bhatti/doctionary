<?php

namespace Modules\Doctor\Http\Controllers\user_dashboard;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;

class DoctorController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        return view('doctor::user_dashboard.doctor_detail');
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */

     //--------Start functions of Manage labs------
    public function appointment()
    {
        return view('doctor::user_dashboard.doctor_detail.appointment');
    }
  

    public function appointment_history()
    {
        return view('doctor::user_dashboard.doctor_detail.appointment_history');
    }
     public function upload_doc()
    {
        return view('doctor::user_dashboard.doctor_detail.upload_doc');
    }
//--------End function of Manage labs------


    public function book_appointment()
    {
        return view('doctor::user_dashboard.book_appointment');
    }

    public function create()
    {
        return view('doctor::create');
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Show the specified resource.
     * @param int $id
     * @return Response
     */
    public function show($id)
    {
        return view('doctor::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Response
     */
    public function edit($id)
    {
        return view('doctor::edit');
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Response
     */
    public function destroy($id)
    {
        //
    }
}
