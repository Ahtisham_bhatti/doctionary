<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::prefix('doctor')->group(function() {
   // Route::get('/details', 'user_dashboard\DoctorController@index')->name('doctor.detail');

	 Route::get('/appointment', 'user_dashboard\DoctorController@appointment')->name('doctor.appointment');
      
    Route::get('/appointment_history', 'user_dashboard\DoctorController@appointment_history')->name('doctor.appointment_history');
     Route::get('/upload', 'user_dashboard\DoctorController@upload_doc')->name('doctor.upload');


});

//for Book Appointment
Route::get('doctors/book_appointment', 'user_dashboard\DoctorController@book_appointment')->name('doctor.book_appointment');

// Website...................................................................................


Route::get('/doctors', 'website\DoctorController@index')->name('web.doctors');
Route::get('/doctors/detail', 'website\DoctorController@doctor_detail')->name('web.doctor_detail');