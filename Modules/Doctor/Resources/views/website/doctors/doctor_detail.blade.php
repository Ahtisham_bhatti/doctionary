@extends('frontend.master')

@section('content')

<!-- Mobile Menu Start -->
	@include('frontend.layouts.mobile_menu')
    <!-- Mobile Menu End -->

	
   <!--Start Banner-->
   
   <div class="sub-banner">
   
   	<img class="banner-img" src="{{URL::to('frontend/images/sub-banner.jpg')}}" alt="">
    <div class="detail">
    	<div class="container">
        	<div class="row">
            	<div class="col-md-12">
                	
                    <div class="paging">
                		<h2>Doctor Detail 
                		<a href="{{route('doctor.book_appointment')}}"  class="btn btn-primary btn pull-right" role="button" style="font-size: 20px">Book Appointment</a></h2>
						<ul>
						<li><a href="{{route('home')}}">Home</a></li>
						<li><a href="{{route('web.doctors')}}">Doctors</a></li>
						<li><a>Doctor detial</a></li>
						</ul>
                    </div>
                    
                </div>
            </div>
        </div>
    </div>
   
   </div>	
   
   <!--End Banner-->
   
   
   
   <!--Start Content-->
   <div class="content">
   
   
   <!--Start Team Detail-->
		<div class="member-detail">
			<div class="container">
			
				
				<div class="row">
				
					
					<div class="col-md-5">
						<img src="{{URL::to('frontend/images/team-member-detail2.jpg')}}" alt="">
					</div>
					
					<div class="col-md-7">
						<div class="team-detail">
							
							<div class="name">
								<h6>Dr.Andrew Bert</h6>
								<span>Outpatient Surgery</span>
							</div>
							
								<ul>
									<li><span class="title">Speciality</span> <span>Physiotherapist</span></li>
									<li><span class="title">Degrees</span> <span>M.D. of Medicine</span></li>
									<li><span class="title">Experience</span> <span>38 years of Experience</span></li>
									<li><span class="title">Training</span> <span>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Voluptate velit esse cillum dolore eu fugiat nulla pariatur. Lorem ipsum dolor sit amet, consectetur adipisicing elit.</span></li>
									
									
								</ul>

							
						</div>


					</div>
					
				
				</div>
				
			</div>
		</div>
   <!--End Team Detail-->
   
   
   </div>
   <!--End Content-->
@stop