<?php

namespace App\Http\Controllers\Auth;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
Use Auth;

class AdminLoginController extends Controller
{

	public function __construct()
	{
		$this->middleware('guest:admin',['except'=>['adminlogout']]);
	}

   	public function showLoginForm()
    {

    	return view('auth.admin_login');
    }
    public function login(Request $request)
    {

      //return $request->all();
    	

      $this->validate($request, [
          'email' => 'required|email',
          'password'=>'required|min:6',
      ]);
	     	

      // Auth::guard('admin')->attempt($credentials,$remember);
       if(Auth::guard('admin')->attempt(['email'=>$request->email,'password'=>$request->password],$request->remember))
       {
        return redirect()->intended(route('admin.dashboard'));
       }
       else
       {
        return redirect()->back()->withInput($request->only('email','remember'));
       }
       
    }
    public function adminlogout()
    {
       
      Auth::guard('admin')->logout();
        return  redirect('/');
    }
}
