<?php

namespace App\Http\Controllers\Auth;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
Use Auth;

class SuperAdminLoginController extends Controller
{

	public function __construct()
	{
		$this->middleware('guest:super_admin',['except'=>['super_adminlogout']]);
	}

   	public function showLoginForm()
    {

    	return view('auth.Super_admin_login');
    }
    public function login(Request $request)
    {

  
    	

      $this->validate($request, [
          'email' => 'required|email',
          'password'=>'required|min:6',
      ]);
	     	

      // Auth::guard('super_admin')->attempt($credentials,$remember);
       if(Auth::guard('super_admin')->attempt(['email'=>$request->email,'password'=>$request->password],$request->remember))
       {
       // return $request->all(); 
        return redirect()->intended(route('super_admin.dashboard'));
       }
       else
       {
        return redirect()->back()->withInput($request->only('email','remember'));
       }
       
    }
    public function super_adminlogout()
    {

        Auth::guard('super_admin')->logout();
        return  redirect('/');

    }
}
