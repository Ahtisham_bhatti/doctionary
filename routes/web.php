<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });
//Route::get('/dashboard','MainController@index')->name('main.page');
Route::get('/profile','MainController@profile')->name('profile.page');
//Route::get('/dashboard/charts','adminController@charts')->name('admin.charts');

Auth::routes();
//for User
Route::get('/', 'HomeController@index')->name('home');

Route::get('/users/logout','Auth\LoginController@userlogout')->name('user.logout');

// website Contact.............................................
Route::get('/contact-us', 'HomeController@contact')->name('web.contact');
// for Admin route make function
Route::prefix('admin')->group(function(){
Route::get('/', 'AdminController@index')->name('admin.dashboard');
Route::get('/login', 'Auth\AdminLoginController@showLoginForm')->name('admin.login');
Route::post('/login', 'Auth\AdminLoginController@login')->name('admin.login.submit');
Route::get('/logout','Auth\AdminLoginController@adminlogout')->name('admin.logout');

});

Route::prefix('super_admin')->group(function(){
Route::get('/', 'SuperAdminController@index')->name('super_admin.dashboard');
Route::get('/login', 'Auth\SuperAdminLoginController@showLoginForm')->name('super_admin.login');
Route::post('/login', 'Auth\SuperAdminLoginController@login')->name('super_admin.login.submit');
Route::get('/logout','Auth\SuperAdminLoginController@super_adminlogout')->name('super_admin.logout');

});
